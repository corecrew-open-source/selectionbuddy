<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Default step entities.
 *
 * @ingroup general
 */
class DefaultStepDeleteForm extends ContentEntityDeleteForm {


}
