<?php

/**
 * Implements hook_token_info().
 */
function general_token_info() {
  $list = general_get_contententitytype_id();
  foreach ($list as $entity_id) {
    $info['tokens'][$entity_id]['uuid'] = [
      'name' => t('@entity_id UUID', ['@entity_id' => $entity_id]),
      'description' => t('The Universal Unique Identifier of @entity_id', ['@entity_id' => $entity_id]),
    ];
  }
  return $info;
}

/**
 * Implements hook_tokens().
 *
 * @param string $type machine name of the group (type)
 * @param array $tokens tokens to be replaced.
 * @param array $data An associative array of data objects
 * @param array $options An associative array of options for token replacement
 * @param \Drupal\Core\Render\BubbleableMetadata $bubbleableMetadata
 *
 * @return array  associative keyed by [type:token] strings
 */
function general_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleableMetadata) {
  foreach (general_get_contententitytype_id() as $entity_id) {
    if ($type == $entity_id && !empty($data[$entity_id])) {
      $entity = $data[$entity_id];
      $replaced = [];
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'uuid':
            $replaced[$original] = $entity->uuid();
            break;
        }
      }
      return $replaced;
    }
  }
}

/**
 * @return array  list of entities type IDs keyed by entity group
 */
function general_get_contententitytype_id() {
  $entity_types = [];
  $definitions = \Drupal::entityTypeManager()->getDefinitions();
  foreach ($definitions as $definition) {
    if ($definition instanceof Drupal\Core\Entity\ContentEntityType) {
      $entity_types[] = $definition->id();
    }
  }
  return $entity_types;
}
