<?php

namespace Drupal\general;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for sjt_result.
 */
class SJTresultTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
