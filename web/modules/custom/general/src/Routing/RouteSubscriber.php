<?php
namespace Drupal\general\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscribe to the Route to change page titles.
 */
class RouteSubscriber extends RouteSubscriberBase {

  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.job.add_form')) {
      // set the title (or override '_title_callback' below)
      $route->setDefault('_title', (string) t('Maak nieuwe vacature aan'));
      // unset the _title_callback; alternatively you could override it here
      $route->setDefault('_title_callback', '');
    }
  }

  public function checkAuthStatus(GetResponseEvent $event) {
    $route_name = \Drupal::routeMatch()->getRouteName();
    if ($route_name === 'entity.user.canonical') {
      $account = \Drupal::currentUser();
      if(in_array('company', $account->getRoles())) {
        $redirectRouteName = 'general.user_controller_getCompanyProfilePage';
      } else {
        $redirectRouteName = 'general.user_controller_getEmployeeProfilePage';
      }
      $response = new \Symfony\Component\HttpFoundation\RedirectResponse(Url::fromRoute($redirectRouteName)->toString());
      $event->setResponse($response);
      $event->stopPropagation();
      return;
    }
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkAuthStatus');
    return $events;
  }
}
