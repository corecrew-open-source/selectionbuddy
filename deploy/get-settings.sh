#!/bin/bash

if [ -d "../deploy" ]; then
  # Go the root of the project
  cd ..
fi

source deploy/params.sh
source deploy/functions.sh

echo ''
echo ''
echo '----------------------------------------------------------'
pause 'Retrieve the settings file from production [Enter]'

# Retrieve the config and overwrite your local config
scp -r ${SERVER_USER}@${SERVER_DOMAIN}:${SERVER_PATH}/web/sites/default/settings.php web/sites/default/

echo ''
echo '----------------------------------------------------------'
echo 'Settings copied'
