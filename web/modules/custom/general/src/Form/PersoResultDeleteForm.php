<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Perso result entities.
 *
 * @ingroup general
 */
class PersoResultDeleteForm extends ContentEntityDeleteForm {


}
