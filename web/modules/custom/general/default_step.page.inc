<?php

/**
 * @file
 * Contains default_step.page.inc.
 *
 * Page callback for Default step entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Default step templates.
 *
 * Default template: default_step.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_default_step(array &$variables) {
  // Fetch DefaultStep Entity Object.
  $default_step = $variables['elements']['#default_step'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
