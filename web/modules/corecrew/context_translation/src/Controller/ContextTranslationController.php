<?php

namespace Drupal\context_translation\Controller;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Controller\ControllerBase;

/**
 * Return response for manual check translations.
 */
class ContextTranslationController extends ControllerBase {

  use ContextStringTranslationTrait;

  /**
   * Shows the string search screen.
   *
   * @return array
   *   The render array for the string search screen.
   */
  public function translatePage() {
    return [
      'filter' => $this->formBuilder()->getForm('Drupal\context_translation\Form\ContextTranslationTranslateFilterForm'),
      'form' => $this->formBuilder()->getForm('Drupal\context_translation\Form\ContextTranslationTranslateEditForm'),
    ];
  }
}
