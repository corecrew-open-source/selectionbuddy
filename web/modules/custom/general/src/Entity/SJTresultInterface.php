<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Sjtresult entities.
 *
 * @ingroup general
 */
interface SJTresultInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Sjtresult name.
   *
   * @return string
   *   Name of the Sjtresult.
   */
  public function getName();

  /**
   * Sets the Sjtresult name.
   *
   * @param string $name
   *   The Sjtresult name.
   *
   * @return \Drupal\general\Entity\SJTresultInterface
   *   The called Sjtresult entity.
   */
  public function setName($name);

  /**
   * Gets the Sjtresult creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Sjtresult.
   */
  public function getCreatedTime();

  /**
   * Sets the Sjtresult creation timestamp.
   *
   * @param int $timestamp
   *   The Sjtresult creation timestamp.
   *
   * @return \Drupal\general\Entity\SJTresultInterface
   *   The called Sjtresult entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Sjtresult published status indicator.
   *
   * Unpublished Sjtresult are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Sjtresult is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Sjtresult.
   *
   * @param bool $published
   *   TRUE to set this Sjtresult to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\general\Entity\SJTresultInterface
   *   The called Sjtresult entity.
   */
  public function setPublished($published);

}
