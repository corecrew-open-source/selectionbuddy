<?php

namespace Drupal\general;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Perso result entity.
 *
 * @see \Drupal\general\Entity\PersoResult.
 */
class PersoResultAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\general\Entity\PersoResultInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished perso result entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published perso result entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit perso result entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete perso result entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add perso result entities');
  }

}
