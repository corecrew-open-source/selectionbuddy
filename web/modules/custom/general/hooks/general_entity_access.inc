<?php

use \Drupal\Core\Entity\EntityInterface;
use \Drupal\Core\Session\AccountInterface;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Implements HOOK_entity_access().
 */
function general_entity_access(EntityInterface $entity, $operation, AccountInterface $account)
{
  if ($entity->getEntityTypeId() === 'node') {
    if ($entity->bundle() === 'page') {
      $menu_link_manager = \Drupal::service('plugin.manager.menu.link');

      $node_id = $entity->id();
      $menu_link = $menu_link_manager->loadLinksByRoute('entity.node.canonical', array('node' => $node_id));
      if (is_array($menu_link) && count($menu_link)) {
        $menu_link = reset($menu_link);
        $tipsId = 'menu_link_content:41701e9c-4b0a-4803-bf30-116272c492da';
        if ($menu_link->getPluginId() === $tipsId && $account->isAnonymous()) {
          $response = new RedirectResponse(Url::fromRoute('<front>')->toString());
          $response->send();
        }
        $menu_link_parents = $menu_link->getParent();
        if ($menu_link_parents) {
          $parents = $menu_link_manager->getParentIds($menu_link->getParent());
          $reversed = array_reverse($parents);
          $parent = reset($reversed);
          if ($parent === $tipsId && $account->isAnonymous()) {
            $response = new RedirectResponse(Url::fromRoute('<front>')->toString());
            $response->send();
          }
        }
      }
    }
  }
}
