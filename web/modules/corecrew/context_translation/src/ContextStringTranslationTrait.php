<?php

namespace Drupal\context_translation;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

trait ContextStringTranslationTrait {

  use StringTranslationTrait;

  protected function t_ct($string, array $args = [], array $options = []) {
    $options['context'] = 'Corecrew';
    return new TranslatableMarkup($string, $args, $options, $this->getStringTranslation());
  }
}
