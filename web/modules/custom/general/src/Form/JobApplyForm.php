<?php

namespace Drupal\general\Form;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\general\Entity\Application;
use Drupal\general\Entity\DefaultStep;
use Drupal\general\Entity\Job;
use Drupal\general\Entity\MotivationCV;
use Drupal\general\Entity\Value;
use Drupal\general\Plugin\QueryHelper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class JobApplyForm.
 */
class JobApplyForm extends FormBase
{

  use ContextStringTranslationTrait;

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var MailManager
   */
  protected $mailManager;

  /**
   * Class constructor.
   * @param AccountInterface $account
   * @param LanguageManagerInterface $languageManager
   * @param QueryHelper $queryHelper
   * @param EntityRepositoryInterface $entityRepository
   * @param RequestStack $requestStack
   * @param MailManager $mailManager
   */
  public function __construct(AccountInterface $account, LanguageManagerInterface $languageManager, QueryHelper $queryHelper, EntityRepositoryInterface $entityRepository, RequestStack $requestStack, MailManager $mailManager)
  {
    $this->account = $account;
    $this->languageManager = $languageManager;
    $this->queryHelper = $queryHelper;
    $this->entityRepository = $entityRepository;
    $this->request = $requestStack->getCurrentRequest();
    $this->mailManager = $mailManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('general.query_helper'),
      $container->get('entity.repository'),
      $container->get('request_stack'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'job_apply_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form_state->disableCache();
    if ($this->account->id() === 0 || !in_array('employe', $this->account->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    $jobId = $this->request->get('id');
    if ($jobId !== NULL) {
      $jobEnityId = $this->queryHelper->getJobByUniqueId($jobId);
      $job = Job::load(reset($jobEnityId));

      $anon = $this->queryHelper->isJobAnon($job->id(), 'job');

      $form['job_id'] = [
        '#type' => 'hidden',
        '#value' => reset($jobEnityId),
      ];
      $form['forma_motivation'] = [
        '#type' => 'textarea',
        '#title' => $this->t_ct('Motivatie'),
        '#description' => $this->t_ct('Waarom ben jij de geschikte kandidaat? Waarom wil je hier werken?'),
        '#maxlength' => 255,
        '#size' => 255,
        '#weight' => '0',
        '#required' => $anon
      ];
      $form['forma_experience_extra'] = [
        '#type' => 'textarea',
        '#title' => $this->t_ct('Werkervaring'),
        '#description' => $this->t_ct('Welke relevante ervaring heb je?'),
        '#maxlength' => 255,
        '#size' => 255,
        '#weight' => '0',
        '#required' => $anon
      ];
      $form['forma_diploma_extra'] = [
        '#type' => 'textarea',
        '#title' => $this->t_ct('Diploma\'s en certificaten'),
        '#description' => $this->t_ct('Over welke relevante diploma’s en/of certificaten of attesten beschik je?'),
        '#maxlength' => 255,
        '#size' => 255,
        '#weight' => '0',
        '#required' => $anon
      ];

      $form['extra'] = [
        '#markup' => '<p>' . $this->t_ct('De werkgever koos ervoor om nog enkele specifieke zaken te bevragen, deze vindt u hieronder.') . '<h3>' . $this->t_ct('Extra gegevens') . '</h3></p>',
      ];

      $languages = [];
      if (isset($job->field_job_languages->getValue()[0])) {
        $languages = $job->field_job_languages->getValue();
      }
      if (count($languages) > 0) {
        $langNames = $this->languageManager->getStandardLanguageList();
        foreach ($languages as $lang) {
          $form['formb_lang_' . $lang['value']] = [
            '#type' => 'radios',
            '#title' => $langNames[$lang['value']][1],
            '#options' => $this->queryHelper->getTaxonomyFormArr('language_level'),
            '#required' => TRUE,
          ];
        }
        $form['divider_1'] = [
          '#markup' => '<div class="divider">&nbsp;</div>',
        ];
      }

      $form['formc_diploma'] = [
        '#type' => 'radios',
        '#title' => $this->t('Niveau diploma'),
        '#options' => $this->queryHelper->getTaxonomyFormArr('diploma'),
      ];

      $form['divider_3'] = [
        '#markup' => '<div class="divider">&nbsp;</div>',
      ];

      $experience = $job->field_job_experience->value;
      if ($experience === '1') {
        $form['formd_experience'] = [
          '#type' => 'radios',
          '#title' => $this->t('Ervaring'),
          '#options' => $this->queryHelper->getTaxonomyFormArr('ervaringsniveau'),
          '#required' => TRUE,
        ];
        $form['divider_4'] = [
          '#markup' => '<div class="divider">&nbsp;</div>',
        ];
      }

      $driver = $job->field_job_driver->value;
      if ($driver === '1') {
        $form['formd_driver'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Rijbewijs'),
          '#options' => $this->queryHelper->getTaxonomyFormArr('rijbewijs'),
          '#required' => TRUE,
        ];
        $form['divider_5'] = [
          '#markup' => '<div class="divider">&nbsp;</div>',
        ];
      }

      // Extra fields.
      $extraFields = [];
      if (isset($job->field_job_extra_fields->getValue()[0])) {
        $extraFields = $job->field_job_extra_fields->getValue();
      }
      if (count($extraFields) > 0) {
        foreach ($extraFields as $index => $extraField) {
          $form['form_extrafields_' . $index] = [
            '#type' => 'textarea',
            '#title' => $extraField['value'],
            '#maxlength' => 255,
            '#size' => 255,
            '#weight' => '0',
            '#required' => TRUE,
          ];
        }
      }

      $form['forma_cv'] = [
        '#type' => 'managed_file',
        '#title' => t('Mijn cv') . ' *',
        '#upload_validators' => [
          'file_validate_extensions' => ['pdf'],
          'file_validate_size' => [600000],
        ],
        '#upload_location' => 'public://cv',
        '#required' => FALSE,
      ];

      $form['anonymous_info'] = [
        '#markup' => '<p>' . $this->t('* Gegevens kunnen niet gebruikt worden bij het anoniem solliciteren') . '</p>',
      ];

      $form['required_info'] = [
        '#markup' => '<p><span class="form-required"></span>' . $this->t('Verplicht in te vullen') . '</p>',
      ];

      // Check if waardenfit is enabled.
      $form['job_valuecompare'] = [
        '#type' => 'hidden',
        '#value' => $job->field_job_valuecompare->value,
      ];
      if ($job->field_job_valuecompare->value === '1') {
        // Add the legend to the side of the screen.
        $legendArr = [
          1 => $this->t('Niet belangrijk'),
          2 => $this->t('Minder belangrijk'),
          3 => $this->t('Neutraal'),
          4 => $this->t('Belangrijk'),
          5 => $this->t('Heel belangrijk'),
        ];
        $buildLegend = '';
        foreach ($legendArr as $key => $element) {
          $buildLegend .= '<li class="star-' . $key . '">';
          $buildLegend .= $element;
          $buildLegend .= '</li>';
        }

        $form['legend'] = [
          '#markup' => '<ul class="legend">' . $buildLegend . '</ul>',
        ];

        $form['container_values'] = [
          '#type' => 'container',
        ];
        $form['container_values']['info_test'] = [
          '#markup' => '<h3>' . $this->t('Mijn waarden') . '</h3><p>' . $this->t('Omdat niet enkel competenties en diploma’s belangrijk zijn om na te gaan of jij de geschikte kandidaat bent voor de job, wordt hieronder gevraagd om aan te duiden hoe belangrijk onderstaande zaken zijn voor jou in je job of in de werkcontext. Op die manier streven we ernaar dat jij en je toekomstige werkgever op dezelfde golflengte zitten! ') . '</p>',
        ];

        // Get the values
        $currentLanguage = $this->languageManager->getCurrentLanguage()->getId();
        $valueIds = $this->queryHelper->getValuesForCurrentUser();
        $values = Value::loadMultiple($valueIds);
        foreach ($values as $vid => $value) {
          if ($value->hasTranslation($currentLanguage)) {
            $translatedValue = $this->entityRepository->getTranslationFromContext($value, $currentLanguage);
            $term = $translatedValue->field_value_term->referencedEntities();
            $translatedTerm = $this->entityRepository->getTranslationFromContext($term[0], $currentLanguage);

            $form['container_values']['value-' . $vid] = [
              '#type' => 'container',
            ];
            $form['container_values']['value-' . $vid]['rating-' . $vid] = [
              '#type' => 'radios',
              '#title' => $translatedTerm->label(),
              '#options' => [
                1 => '1',
                2 => '2',
                3 => '3',
                4 => '4',
                5 => '5',
              ],
              '#default_value' => $translatedValue->get('field_value_rating')->getValue()[0]['value'],
              '#description' => $translatedTerm->field_body_employee->value,
            ];
          }
        }
      }
    }

    if ($anon) {
      $form['extra_anon'] = [
        '#markup' => '<div class="job-anon"><h3>' . t('Anoniem solliciteren') . '</h3><p>' . $this->t_ct('Het bedrijf waarvoor u solliciteert, heeft ervoor geopteerd om de eerste selectieronde anoniem te laten verlopen. Dit kadert binnen diversiteits- en gelijke kansenbeleid. Concreet betekent het dat zij in de eerste selectieronde geen kennis hebben van je naam, je geslacht, je leeftijd en andere persoonlijke gegevens. Op die manier proberen ze zo objectief mogelijk alle kandidaturen te screenen. In de tweede selectieronde worden jouw gegevens wel aan hen vrijgegeven. Telkens wanneer u een sterretje ziet staan bij een invulvak, betekent dit dus dat je nu dit vak reeds dient in te vullen, maar dat het pas in de tweede selectieronde zichtbaar gemaakt wordt voor de werkgever.') . '</p>',
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Verstuur'),
      '#submit' => array('::submitForm'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    // Get the current application by the jobId and the userid.
    $jobEntity = Job::load($values['job_id']);

    // Create the application for the current user linked to correct job.
    // Check if the user has already an application for the current job.
    $jobApplications = $jobEntity->field_job_applications->referencedEntities();
    if (count($jobApplications) > 0) {
      $userArr = [];
      foreach ($jobApplications as $application) {
        $applicationUser = $application->get('user_id')->getValue()[0]['target_id'];
        $userArr[] = $applicationUser;
      }
      if (in_array($this->account->id(), $userArr) === FALSE) {
        $applicationValues = [
          'status' => 1,
          'name' => $jobEntity->id() . ' application',
          'field_app_active' => 1,
          'field_app_date' => (new \DateTime('now'))->format('Y-m-d'),
          'field_app_current_step_index' => 1,
        ];
        $application = Application::create($applicationValues);
        $application->setOwner(User::load($this->account->id()));
        $application->save();

        $jobEntity->field_job_applications->appendItem($application);
        $jobEntity->save();
      }
    } else {
      $applicationValues = [
        'status' => 1,
        'name' => $jobEntity->id() . ' application',
        'field_app_active' => 1,
        'field_app_date' => (new \DateTime('now'))->format('Y-m-d'),
        'field_app_current_step_index' => 1,
      ];
      $application = Application::create($applicationValues);
      $application->setOwner(User::load($this->account->id()));
      $application->save();

      $jobEntity->field_job_applications->appendItem($application);
      $jobEntity->save();
    }

    // Create the step en mark it as current.
    $stepValues = [
      'status' => 1,
      'uid' => $this->account->id(),
      'name' => 'job: ' . $values['job_id'] . ' - user: ' . $this->account->id() . ' - application: ' . $application->id(),
    ];

    $stepValues['field_step1_languages'] = '<ul>';
    $stepValues['field_step1_values'] = [];
    $valueUserArr = [];
    $langNames = $this->languageManager->getStandardLanguageList();
    $extraFieldsData = [];

    foreach ($values as $key => $value) {
      if (strpos($key, 'forma_') !== FALSE) {
        $stepValues[str_replace('forma_', 'field_step1_', $key)] = $value;
      }
      if (strpos($key, 'formc_') !== FALSE) {
        $stepValues[str_replace('formc_', 'field_step1_', $key)] = ['target_id' => $value];
      }
      if (strpos($key, 'formd_') !== FALSE) {
        $valToSave = ['target_id' => $value];
        if (is_array($value)) {
          $valToSave = [];
          foreach ($value as $id => $driver) {
            if ($driver !== 0) {
              $valToSave[] = ['target_id' => $id];
            }
          }
        }
        $stepValues[str_replace('formd_', 'field_step1_', $key)] = $valToSave;
      }
      if (strpos($key, 'formb_lang_') !== FALSE) {
        $langId = str_replace('formb_lang_', '', $key);
        $langName = $langNames[$langId][1];
        $valueName = Term::load($value)->label();
        $stepValues['field_step1_languages'] .= '<li><span class="lang">' . $langName . '</span>: ' . $valueName . '</li>';
      }

      // Check if 'waardenfit' is enabled.
      if ($values['job_valuecompare'] === '1') {
        // Copy the values and calculate (field_step1_value_score) the score.
        if (strpos($key, 'rating-') !== FALSE) {
          // Copy the values
          $valueEntity = Value::load(str_replace('rating-', '', $key));
          $ratingTerm = $valueEntity->field_value_term->getValue();
          $newValue = Value::create([
            'status' => 1,
            'uid' => $this->account->id(),
            'field_value_term' => $ratingTerm,
            'field_value_rating' => $value,
            'field_value_jobvalue' => 1,
          ]);
          $newValue->save();
          $valueUserArr[$ratingTerm[0]['target_id']] = $value;

          // Link to the current step (field_step1_values).
          $stepValues['field_step1_values'][] = ['target_id' => $newValue->id()];
        }
      }

      if (strpos($key, 'form_extrafields_') !== FALSE) {
        $fieldIndex = str_replace('form_extrafields_', '', $key);
        $extraFieldsData[$fieldIndex] = $value;
      }
    }

    $companyUserId = $jobEntity->getOwnerId();
    // Check if "waardenfit" is enabled.
    if ($values['job_valuecompare'] === '1') {
      // Get the value from the company user.
      $valueIds = $this->queryHelper->getValuesForCurrentUser($companyUserId);
      $valueCompanyEntities = Value::loadMultiple($valueIds);
      $valueComanyArr = [];
      foreach ($valueCompanyEntities as $valueCompanyEntity) {
        $valueComanyArr[$valueCompanyEntity->field_value_term->getValue()[0]['target_id']] = $valueCompanyEntity->field_value_rating->value;
      }
      $stepValues['field_step1_languages'] .= '</ul>';

      // Calculate the score.
      $differenceMappingArr = [
        5 => 0,
        4 => 0,
        3 => 0.25,
        2 => 0.5,
        1 => 0.75,
        0 => 1,
      ];
      $score = 0;
      $totalValues = count($valueUserArr);
      foreach ($valueUserArr as $termId => $userValue) {
        $calc = abs((int)$userValue - (int)$valueComanyArr[$termId]);
        $score += $differenceMappingArr[$calc];
      }
      $score = round(($score / $totalValues) * 100, 1);
      $stepValues['field_step1_value_score'] = $score;
      $application->set('field_app_valuecompare_score', $score);
      $application->save();
    } else {
      $stepValues['field_step1_value_score'] = $this->t('nvt');
    }

    // Link the created 'motivation' entity to the 'application' entity.
    $step = MotivationCV::create($stepValues);

    // Save the extra fields.
    if (count($extraFieldsData) > 0) {
      $step->set('field_step1_extra_fields', json_encode($extraFieldsData));
    }

    // Save the step.
    $step->save();

    // Add all the steps in de all_step field in the form a json
    $stepsArr = [];
    $stepsArr[] = [
      'type' => 'motivation_c_v',
      'id' => $step->id(),
      'label' => 'Motivatie & CV',
    ];

    // Adjust the current step index in de 'application' entity.
    $application->set('field_app_current_step_index', 0);

    // Set the current step id and type
    $application->set('field_app_current_step_id', $step->id());
    $application->set('field_app_current_step_type', 'motivation_c_v');

    // Add the other steps if checked in the current job.
    $acquaintance = $jobEntity->field_job_acquaintance->value;
    if ($acquaintance === '1') {
      // Add the step to the application.
      $step = $this->createStep($application->id());
      $stepsArr[] = [
        'type' => 'default_step',
        'subtype' => 'acc',
        'id' => $step,
        'label' => 'Kennismaking',
      ];
    }

    // Add the tests
    // Add the personalities questions
    if ($jobEntity->field_job_personalities->value === '1') {
      $stepsArr[] = [
        'type' => 'tests',
        'tests' => [
          'perso'
        ]
      ];
    }

    // Add the sjt questions
    if ($jobEntity->field_job_sjt->value === '1') {
      if ($jobEntity->field_job_personalities->value === '1') {
        $stepsArr[count($stepsArr) - 1]['tests'][] = 'sjt';
      } else {
        $stepsArr[] = [
          'type' => 'tests',
          'tests' => [
            'sjt'
          ]
        ];
      }
    }

    // Add the "gesprek 1" & "gesprek 2"
    $talk1 = $jobEntity->field_job_talk_1->value;
    if ($talk1 === '1') {
      // Add the step to the application.
      $step = $this->createStep($application->id());
      $stepsArr[] = [
        'type' => 'default_step',
        'subtype' => 'talk1',
        'id' => $step,
        'label' => 'Gesprek 1',
      ];
    }

    $talk2 = $jobEntity->field_job_talk_2->value;
    if ($talk2 === '1') {
      // Add the step to the application.
      $step = $this->createStep($application->id());
      $stepsArr[] = [
        'type' => 'default_step',
        'id' => $step,
        'subtype' => 'talk2',
        'label' => 'Gesprek 2',
      ];
    }

    // Save all the steps to the application
    $application->set('field_app_all_steps', json_encode($stepsArr));

    // Save the application.
    $application->save();

    // Send mail to company if is checked in company profile
    $user = User::load($this->account->id());
    $name = $user->get('field_emp_firstname')->getValue()[0]['value'] . ' ' . $user->get('field_emp_name')->getValue()[0]['value'];
    $this->sendMail($companyUserId, $jobEntity->label(), $name, $application->id());

    $current_uri = $this->request->getRequestUri();
    $form_state->setRedirectUrl(Url::fromUserInput($current_uri));
  }

  private function createStep($appId)
  {
    $newStep = DefaultStep::create([
      'status' => 1,
      'uid' => $this->account->id(),
      'field_step_application' => ['target_id' => $appId],
    ]);
    $newStep->save();
    return $newStep->id();
  }

  private function sendMail($companyUserId, $job, $applicationUser, $applicationId)
  {
    $company = User::load($companyUserId);
    if (isset($company->get('field_mails')->getValue()[0]) && $company->get('field_mails')->getValue()[0]['value'] === '1') {
      $to = $company->getEmail();

      // Message.
      if ($this->queryHelper->isJobAnon($applicationId)) {
        $params['message'] = $this->t_ct('Beste, <br /><br />Er is een nieuwe sollicitatie voor de vacature @job. <br />Je kan de kandidatuur <a href="/applications/job-detail/@jobId">hier</a> bekijken.', ['@job' => $job, '@user' => $applicationUser, '@jobId' => $applicationId]);
      } else {
        $params['message'] = $this->t_ct('Beste, <br /><br />@user solliciteerde zonet voor de vacature @job. <br />Je kan de kandidatuur <a href="/applications/job-detail/@jobId">hier</a> bekijken.', ['@job' => $job, '@user' => $applicationUser, '@jobId' => $applicationId]);
      }

      $params['subject'] = $this->t_ct('Er is een nieuwe sollicitatie voor @job', ['@job' => $job]);

      // Send mail to employee.
      $result = $this->mailManager->mail('general', 'new_application', $to, 'nl', $params, NULL, TRUE);
      return $result['result'];
    }

    return FALSE;
  }
}
