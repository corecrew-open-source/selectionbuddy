<?php

namespace Drupal\general\Form;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Class UserEmployeRegisterFormStep2.
 */
class UserEmployeeRegisterFormStep2 extends FormBase
{

  use ContextStringTranslationTrait;

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * Class constructor.
   * @param AccountInterface $account
   * @param PrivateTempStoreFactory $temp_store_factory
   * @param SessionManagerInterface $session_manager
   */
  public function __construct(AccountInterface $account, PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager)
  {
    $this->account = $account;
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->store = $this->tempStoreFactory->get('multistep_data_company_register');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('user.private_tempstore'),
      $container->get('session_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'user_employee_form_step_2';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($this->account->id() !== 0) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    // Personal values.
    $form['personal_values'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['personal_values', 'horizontal-tabs']],
    ];
    $form['personal_values']['personal_values_tabs'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['horizontal-tabs-list']],
    ];
    $form['personal_values']['personal_values_tabs']['title_1'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '1.' . $this->t_ct('Basis gegevens'),
      '#attributes' => ['class' => ['horizontal-tab-button']]
    ];
    $form['personal_values']['personal_values_tabs']['title_2'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '2.' . $this->t_ct('Persoonlijke gegevens'),
      '#attributes' => ['class' => ['selected', 'horizontal-tab-button']]
    ];
    $form['personal_values']['personal_values_tabs']['title_3'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '3.' . $this->t_ct('Waarden'),
      '#attributes' => ['class' => ['horizontal-tab-button']]
    ];
    $form['personal_values']['emp_picture'] = [
      '#type' => 'managed_file',
      '#title' => $this->t_ct('Foto (optioneel)'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [2000000],
      ],
      '#theme' => 'image_widget',
      '#preview_image_style' => 'picture',
      '#upload_location' => 'public://profile-pictures',
      '#required' => FALSE,
      '#description' => '<p><strong>' . $this->t_ct('Toegelaten bestandsextensies') . ':</strong> gif, png, jpg, jpeg</br><strong>' . $this->t_ct('Toegelaten bestandsgrootte') . ':</strong> 2MB</p>',
    ];
    $form['personal_values']['extra'] = [
      '#markup' => '<div class="anonymous_info"><p>' . $this->t_ct('Een aantal gegevens kunnen niet bekeken worden door de werkgever bij het anonniem solliciteren.') . '<span>?</span></p>' .
        '<ul>' .
        '<li>' . $this->t_ct('Foto') . '</li>' .
        '<li>' . $this->t_ct('Voornaam') . '</li>' .
        '<li>' . $this->t_ct('Naam') . '</li>' .
        '<li>' . $this->t_ct('Geslacht') . '</li>' .
        '<li>' . $this->t_ct('Link naar je LinkedIn-pagina') . '</li>' .
        '<li>' . $this->t_ct('Leeftijd') . '</li>' .
        '<li>' . $this->t_ct('Emailadres') . '</li>' .
        '</ul></div>',
      '#weight' => '0'
    ];
    $sexoptions = ['M', 'V', 'X'];
    $form['personal_values']['emp_sex'] = [
      '#type' => 'select',
      '#title' => $this->t_ct('Geslacht'),
      '#options' => array_combine($sexoptions, $sexoptions),
      '#required' => TRUE,
    ];
    $form['personal_values']['emp_birth'] = [
      '#type' => 'date',
      '#title' => $this->t_ct('Geboortedatum'),
      '#required' => TRUE,
    ];
    $form['personal_values']['emp_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Woonplaats'),
      '#maxlength' => 64,
      '#size' => 64,
    ];
    $form['personal_values']['emp_linkedin'] = [
      '#type' => 'url',
      '#title' => $this->t_ct('Link naar je LinkedIn-pagina'),
    ];
    $form['personal_values']['emp_phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Telefoonnummer'),
    ];
    $form['personal_values']['emp_diploma'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Hoogst behaalde diploma'),
      '#maxlength' => 64,
      '#size' => 64,
    ];

    $form['personal_values']['required_info'] = [
      '#markup' => '<p><span class="form-required"></span>' . $this->t('Verplicht in te vullen') . '</p>',
    ];

    $form['personal_values']['button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Volgende'),
      '#attributes' => ['class' => ['button-primary']]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $fields = $this->store->get('fields');
    foreach ($form_state->getValues() as $key => $value) {
      $this->store->set($key, $value);
      $fields[] = $key;
    }
    $this->store->set('fields', $fields);
    $form_state->setRedirect('general.user_employee_register_form_step3');
  }
}
