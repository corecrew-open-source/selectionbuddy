<?php

use \Drupal\Core\Url;
use \Drupal\Core\Render\Markup;

/**
 * Implements hook_form_alter().
 */
function general_form_user_login_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');
  $form['formuser'] = [
    '#weight' => -9999,
    '#markup' => '<div id="page-title"><h1 class="werkgevers">' . $contextTranslationService->t_ct('Werkgever') . '</h1><h1 class="werknemers">' . $contextTranslationService->t_ct('Sollicitant') . '</h1></div><div class="vacature"><div class="company-logo"></div><p class="job-title"></p><p class="company-name"></p></div>',
  ];
  $form['title'] = [
    '#weight' => -9998,
    '#markup' => '<h2>' . t('Reeds een login?') . '</h2>',
  ];
  $form['name']['#attributes'] = [
    'placeholder' => t('E-mail'),
  ];
  $form['name']['#description'] = '';
  $form['pass']['#attributes'] = [
    'placeholder' => t('Password')
  ];
  $form['pass']['#description'] = Markup::create('<a href="/user/password">' . $contextTranslationService->t_ct('Wachtwoord vergeten') . '</a>');
  $form['register'] = [
    '#weight' => 9999,
    '#markup' => '<h2>' . $contextTranslationService->t_ct('Nog geen login?') . '</h2><p>' . $contextTranslationService->t_ct('We vragen je wel om eenmalig een profiel aan te maken zodat je eenvoudig kan inloggen en de voortgang van je sollicitie zelf kan opvolgen') . '</p><p><a class="button button-primary register" href="/general/form/user_employee">' . $contextTranslationService->t_ct('Registreer') . '</a></p>',
  ];

  $mail = Drupal::request()->query->get('mail', FALSE);
  if ($mail) {
    drupal_set_message($contextTranslationService->t_ct('Er is een mail verzonden naar @mail om jouw account te activeren', ['@mail' => $mail]));
  }

  $form['#submit'][] = 'general_user_login_submit';

}

/**
 * Implements HOOK_form_alter().
 */
function general_form_user_form_alter(&$form, $form_state, $form_id) {
  $contextTranslationService = \Drupal::service('context_translation.translation');
  $anon = \Drupal::currentUser()->isAnonymous();
  if ($anon === FALSE) {
    if (in_array('company', \Drupal::currentUser()->getRoles())) {
      $url = Url::fromRoute('general.user_company_edit_form');
    } else if (in_array('employe', \Drupal::currentUser()->getRoles())) {
      $url = Url::fromRoute('general.user_employee_edit_form');
    }

    if (isset($url)) {
      $resp = new \Symfony\Component\HttpFoundation\RedirectResponse($url->toString());
      $resp->send();
    }
  } else {
    $form['account']['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h1',
      '#value' => $contextTranslationService->t_ct('Stel uw wachtwoord in'),
      '#weight' => -20,
    ];

    $form['actions']['submit']['#submit'][] = '_general_user_edit_form_submit';
  }
}

/**
 * Implements HOOK_form_alter().
 */
function general_form_user_pass_alter(&$form, $form_state, $form_id)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');
  $form['hidden_field'] = [
    '#weight' => -9999,
    '#type' => 'hidden',
    '#atrributes' => ['autocomplete' => 'off'],
  ];
  $form['title'] = [
    '#weight' => -9998,
    '#markup' => '<div id="page-title"><h1>' . $contextTranslationService->t_ct('Wachtwoord vergeten') . '</h1></div>',
  ];
  $form['name']['#attributes'] = [
    'placeholder' => $contextTranslationService->t_ct('E-mailadres')
  ];
  $form['#attributes'] = ['autocomplete' => 'off'];
  $form['actions']['submit']['#submit'][] = '_general_user_pass_form_submit';
  $form['actions']['submit']['#value'] = $contextTranslationService->t_ct('Mail versturen');
}

/**
 * Implements HOOK_form_alter().
 */
function general_form_taxonomy_term_waarden_form_alter(&$form, $form_state, $form_id) {
  $form['description']['widget'][0]['#title'] = t('Beschrijving werkgever');
}


/**
 * Implements HOOK_form_alter().
 */
function general_form_job_add_form_alter(&$form, $form_state, $form_id)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');
  $form['name']['widget'][0]['#title'] = $contextTranslationService->t_ct('Functie');
  $form['name']['widget'][0]['value']['#title'] = $contextTranslationService->t_ct('Functie');
  $mappingArrayTipsAndTricks = [
    'field_job_acquaintance' => 10,
    'field_job_valuecompare' => 8,
    'field_job_talk_1' => 12,
    'field_job_talk_2' => 13,
    'field_job_personalities' => 11,
    'field_job_sjt' => 15,
  ];
  foreach ($mappingArrayTipsAndTricks as $id => $tooltip) {
    $alias = Drupal::service('general.query_helper')->getAliasByPath('/node/' . $tooltip);
    $form[$id]['widget']['value']['#description'] = '<span class="tips-and-tricks"><a href="' . $alias . '" target="_blank">' . $contextTranslationService->t_ct('Tips & tricks') . '</a></span>';
  }

  $orgText = $form['field_job_anon']['widget']['value']['#description']->__toString();
  $alias = Drupal::service('general.query_helper')->getAliasByPath('/node/' . 17);
  $orgText .= '<span class="tips-and-tricks"><a href="' . $alias . '" target="_blank">' . $contextTranslationService->t_ct('Tips & tricks') . '</a></span>';
  $form['field_job_anon']['widget']['value']['#description'] = $orgText;
}

/**
 * Implements HOOK_form_alter().
 */
function general_form_job_delete_form_alter(&$form, $form_state, $form_id)
{
  $form['#validate'][] = '_general_validate_job_delete_form';
}

function _general_user_edit_form_submit($form, &$form_state)
{
  if (!in_array('webmaster', \Drupal::currentUser()->getRoles()) && \Drupal::currentUser()->id() !== '1') {
    $form_state->setRedirect('<front>');
  }
}

function _general_validate_job_delete_form(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');
  $node = $form_state->getFormObject()->getEntity();
  if (!empty($node->get('field_job_applications')->getValue())) {
    $form_state->setError($form['actions'], $contextTranslationService->t_ct('Je kan een job enkel verwijderen als deze geen solicitaties bevat.'));
  }
}

function _general_user_pass_form_submit($form, &$form_state)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');
  $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $account = $form_state->getValue('account');
  // Mail one time login URL and instructions using current language.
  $mail = _user_mail_notify('password_reset', $account, $langcode);
  if (!empty($mail)) {
    Drupal::logger('user')->notice('Password reset instructions mailed to %name at %email.', ['%name' => $account->getUsername(), '%email' => $account->getEmail()]);
    drupal_set_message($contextTranslationService->t_ct('Verdere instructies zijn verzonden naar jouw e-mailadres'));
  } else {
    drupal_set_message($contextTranslationService->t_ct('Er ging iets mis.'));
  }

  if (Drupal::currentUser()->id() !== 0) {
    user_logout();
  }
  $form_state->setRedirect('user.login');
}

/**
 * Form submission handler for user_login_form().
 *
 * Redirects the user to the dashboard after logging in.
 */
function general_user_login_submit(&$form, \Drupal\Core\Form\FormStateInterface $form_state)
{
  $url = \Drupal\Core\Url::fromRoute('<front>');

  // Check if a destination was set, probably on an exception controller.
  // @see \Drupal\user\Form\UserLoginForm::submitForm()
  $request = \Drupal::service('request_stack')->getCurrentRequest();
  if (!$request->request->has('destination')) {
    if (in_array('company', \Drupal::currentUser()->getRoles())) {
      $form_state->setRedirectUrl(Url::fromUserInput('/jobs'));
    } else if (in_array('employe', \Drupal::currentUser()->getRoles())) {
      $form_state->setRedirectUrl(Url::fromUserInput('/applications-employee'));
    } else {
      $form_state->setRedirectUrl($url);
    }
  } else {
    $request->query->set('destination', $request->request->get('destination'));
  }
}

function general_form_job_edit_form_alter(&$form, $form_state, $form_id)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');

  $stay = [
    'field_job_desc',
    'field_job_location',
    'field_job_contracttype',
    'field_job_diploma',
  ];
  $disable = [
    'field_job_valuecompare',
    'field_job_acquaintance',
    'field_job_personalities',
    'field_job_sjt',
    'field_job_sjt_categories',
    'field_job_talk_1',
    'field_job_talk_2'
  ];
  foreach ($form as $name => $fields) {
    if (in_array($name, $disable) && isset($form[$name]['widget']['value'])
      && $form[$name]['widget']['value']['#default_value'] === TRUE) {
      if (!isset($form['steps'])) {
        $form['steps'] = [
          '#markup' => '<div class="chosen-steps"><p>' . $contextTranslationService->t_ct('Gekozen stappen') . '</p><ul>',
        ];
      }
      $form['steps']['#markup'] .= '<li>' . $form[$name]['widget']['#title'] . '</li>';
    }
    if (strpos($name, 'field_job') !== FALSE && !in_array($name, $stay)) {
      unset($form[$name]);
    }
  }

  if (isset($form['steps'])) {
    $form['steps']['#markup'] .= '</ul></div>';
  }
  unset($form['selectievragen']);
  unset($form['open_vragen']);
  unset($form['steps-info']);
  unset($form['extra_experience_driver']);

  $form['actions']['submit']['#submit'][] = '_general_job_edit_form_submit';
}

function _general_job_edit_form_submit(&$form, $form_state)
{
  $form_state->setRedirectUrl(Url::fromUserInput('/jobs'));
}

function validate_values(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');
  $values = $form_state->getValues();
  if ($values['field_job_valuecompare']['value'] === 1) {
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    if (isset($user->get('field_company_values_adjusted')->getValue()[0]) && $user->get('field_company_values_adjusted')->getValue()[0]['value'] !== '1') {
      $form_state->setErrorByName('field_job_valuecompare', $contextTranslationService->t_ct('Gelieve eerst jouw bedrijfswaarden in te vullen in jouw bedrijfsprofiel, pas daarna kan je de waardenfit gebruiken.'));
    }
  }
}

function validate_sjt(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');
  $values = $form_state->getValues();
  if ($values['field_job_sjt']['value'] === 1) {
    if (count($values['field_job_sjt_categories']) < 1) {
      $form_state->setErrorByName('field_job_sjt', $contextTranslationService->t_ct('Gelieve de competenties te selecteren die u wil bevragen.'));
    }
  }
}
