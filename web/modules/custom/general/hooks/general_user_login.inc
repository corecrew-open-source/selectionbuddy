<?php

use \Drupal\Core\Url;

/**
 * Implements hook_user_login().
 */
function general_user_login($account) {
  // Redirect to the job if in cookie
  if (isset($_COOKIE['job_id']) && $_COOKIE['job_id'] !== NULL && in_array('employe', $account->getRoles())) {
    // Redirect to job application.
    $response = new \Symfony\Component\HttpFoundation\RedirectResponse(Url::fromRoute('general.application_controller_jobApplication', ['id' => $_COOKIE['job_id']])->toString());
    $response->send();
  }

  if(in_array('company', $account->getRoles())) {
    $response = new \Symfony\Component\HttpFoundation\RedirectResponse(Url::fromRoute('general.user_controller_getCompanyProfilePage')->toString());
    $response->send();
  }
}
