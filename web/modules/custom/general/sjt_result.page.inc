<?php

/**
 * @file
 * Contains sjt_result.page.inc.
 *
 * Page callback for Sjtresult entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Sjtresult templates.
 *
 * Default template: sjt_result.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_sjt_result(array &$variables) {
  // Fetch SJTresult Entity Object.
  $sjt_result = $variables['elements']['#sjt_result'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
