<?php

namespace Drupal\general\TwigExtension;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\general\Form\RatingForm;
use Drupal\general\Plugin\QueryHelper;

/**
 * Class GeneralTwigExtension.
 */
class GeneralTwigExtension extends \Twig_Extension
{


  /**
   * Drupal\Core\Form\FormBuilderInterface definition.
   *
   * @var FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * Constructs a new GeneralTwigExtension object.
   * @param FormBuilderInterface $form_builder
   * @param QueryHelper $queryHelper
   */
  public function __construct(FormBuilderInterface $form_builder, QueryHelper $queryHelper)
  {
    $this->formBuilder = $form_builder;
    $this->queryHelper = $queryHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenParsers()
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeVisitors()
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters()
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTests()
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions()
  {
    return [
      new \Twig_SimpleFunction('rating_form', [$this, 'getRatingForm']),
      new \Twig_SimpleFunction('alias_by_path', [$this, 'getAliasByPath']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getOperators()
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'general.twig.extension';
  }

  public function getRatingForm($id)
  {
    $form = $this->formBuilder->getForm(RatingForm::class, $id);
    return $form;
  }

  public function getAliasByPath($path) {
    return $this->queryHelper->getAliasByPath($path);
  }
}
