<?php

namespace Drupal\general\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ProfileBlock' block.
 *
 * @Block(
 *  id = "profile_block",
 *  admin_label = @Translation("Profile block"),
 * )
 */
class ProfileBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var $account \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $account, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $account;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $roles = $this->account->getRoles();
    $user = User::load($this->account->id());
    $name = '';
    $image = NULL;
    if (in_array('employe', $roles) !== FALSE) {
      $name = $user->field_emp_firstname->value . ' ' . $user->field_emp_name->value;
      if (isset($user->get('field_emp_picture')->getValue()[0])) {
        $logoFid = $user->get('field_emp_picture')->getValue()[0]['target_id'];
        if ($logoFid !== NULL) {
          $style = $this->entityTypeManager->getStorage('image_style')->load('picture');
          $image = $style->buildUrl(File::load($logoFid)->getFileUri());
        }
      }
    } else if(in_array('company', $roles) !== FALSE) {
      $name = $user->field_company_company->value;
    }
    return [
      '#theme' => 'profile_block',
      '#username' => $name,
      '#picture' => $image,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
