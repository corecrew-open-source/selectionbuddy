<?php

namespace Drupal\general;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Sjtresult entity.
 *
 * @see \Drupal\general\Entity\SJTresult.
 */
class SJTresultAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\general\Entity\SJTresultInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished sjtresult entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published sjtresult entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit sjtresult entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete sjtresult entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add sjtresult entities');
  }

}
