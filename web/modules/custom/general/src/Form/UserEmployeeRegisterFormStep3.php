<?php

namespace Drupal\general\Form;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\general\Entity\Value;
use Drupal\general\Plugin\QueryHelper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Class UserEmployeRegisterFormStep3.
 */
class UserEmployeeRegisterFormStep3 extends FormBase
{

  use ContextStringTranslationTrait;

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * Class constructor.
   * @param AccountInterface $account
   * @param LanguageManagerInterface $languageManager
   * @param QueryHelper $queryHelper
   * @param EntityRepositoryInterface $entityRepository
   * @param PrivateTempStoreFactory $temp_store_factory
   * @param SessionManagerInterface $session_manager
   */
  public function __construct(AccountInterface $account, LanguageManagerInterface $languageManager, QueryHelper $queryHelper, EntityRepositoryInterface $entityRepository, PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager)
  {
    $this->account = $account;
    $this->languageManager = $languageManager;
    $this->queryHelper = $queryHelper;
    $this->entityRepository = $entityRepository;
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->store = $this->tempStoreFactory->get('multistep_data_company_register');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('general.query_helper'),
      $container->get('entity.repository'),
      $container->get('user.private_tempstore'),
      $container->get('session_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'user_employee_form_step_3';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($this->account->id() !== 0) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    // Values.
    $form['values'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['values', 'horizontal-tabs']],
    ];
    $form['values']['values_tabs'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['horizontal-tabs-list']],
    ];
    $form['values']['values_tabs']['title_1'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '1.' . $this->t('Basis gegevens'),
      '#attributes' => ['class' => ['horizontal-tab-button']]
    ];
    $form['values']['values_tabs']['title_2'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '2.' . $this->t('Persoonlijke gegevens'),
      '#attributes' => ['class' => ['horizontal-tab-button']]
    ];
    $form['values']['values_tabs']['title_3'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '3.' . $this->t('Waarden'),
      '#attributes' => ['class' => ['selected', 'horizontal-tab-button']]
    ];

    // Add the intro.
    $form['values']['intro'] = [
      '#markup' => '<p>' . $this->t('Omdat niet enkel competenties en diploma’s belangrijk zijn om na te gaan of jij de geschikte kandidaat bent voor de job, wordt hieronder gevraagd om aan te duiden hoe belangrijk onderstaande zaken zijn voor jou in je job of in de werkcontext. Op die manier streven we ernaar dat jij en je toekomstige werkgever op dezelfde golflengte zitten! ') . '</p>',
    ];
    $form['values']['intro intro_2'] = [
      '#markup' => '<p class="intro2">' . $this->t('Gegevens kunnen later aangevuld of aangepast worden.') . '</p>',
    ];

    // Add the legend to the side of the screen.
    $legendArr = [
      1 => $this->t_ct('Niet belangrijk'),
      2 => $this->t_ct('Minder belangrijk'),
      3 => $this->t_ct('Neutraal'),
      4 => $this->t_ct('Belangrijk'),
      5 => $this->t_ct('Heel belangrijk'),
    ];
    $buildLegend = '';
    foreach ($legendArr as $key => $element) {
      $buildLegend .= '<li class="star-' . $key . '">';
      $buildLegend .= $element;
      $buildLegend .= '</li>';
    }

    $form['values']['legend'] = [
      '#markup' => '<ul class="legend">' . $buildLegend . '</ul>',
    ];

    // Get the values
    $values = $this->queryHelper->getValuesTaxonomy();
    $valueTerms = Term::loadMultiple($values);
    $currentLang = $this->languageManager->getCurrentLanguage()->getId();
    foreach ($valueTerms as $tid => $value) {
      if ($value->hasTranslation($currentLang)) {
        $translatedTerm = $this->entityRepository->getTranslationFromContext($value, $currentLang);
        $form['values']['value-' . $tid] = [
          '#type' => 'container',
        ];
        $form['values']['value-' . $tid]['rating-' . $tid] = [
          '#type' => 'radios',
          '#title' => $translatedTerm->label(),
          '#options' => [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
          ],
          '#description' => $translatedTerm->field_body_employee->value,
          '#required' => TRUE,
        ];
      }
    }

    $form['values']['required_info'] = [
      '#markup' => '<p><span class="form-required"></span>' . $this->t('Verplicht in te vullen') . '</p>',
    ];

    $form['values']['button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Registreer'),
      '#attributes' => ['class' => ['button-primary']]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $fields = $this->store->get('fields');
    $values = [];
    foreach ($fields as $field) {
      $values[$field] = $this->store->get($field);
    }
    $values = array_merge($values, $form_state->getValues());
    $valueTermIds = [];
    $valueRating = [];

    // Create user.
    $userValues = [
      'status' => 1,
      'name' => $values['mail'],
      'mail' => $values['mail'],
    ];

    foreach ($values as $key => $value) {
      if (strpos($key, 'emp_') !== FALSE) {
        $userValues[str_replace('emp_', 'field_emp_', $key)] = $value;
      }
      if (strpos($key, 'rating-') !== FALSE) {
        $id = str_replace('rating-', '', $key);
        $valueTermIds[$id] = $id;
        $valueRating[$id] = $value;
      }
    }

    $user = User::create($userValues);
    $user->setPassword($values['password']);
    $user->addRole('employe');
    $user->enforceIsNew();
    $user->save();

    // Create the values entities for the current user.
    $terms = Term::loadMultiple($valueTermIds);
    foreach ($terms as $term) {
      // Create them for every language.
      $translatedTerm = $this->entityRepository->getTranslationFromContext($term, $this->languageManager->getCurrentLanguage()->getId());
      $valueEntity = Value::create([
        'status' => 1,
        'name' => $translatedTerm->getName(),
        'field_value_rating' => $valueRating[$term->id()],
        'field_value_term' => ['target_id' => $term->id()],
      ]);
      $valueEntity->setOwner($user);
      $valueEntity->save();
    }

    // Send an email to the user.
    _user_mail_notify('register_no_approval_required', $user);

    // Redirect does not matter is in hook_user_login().
    $response = new RedirectResponse(Url::fromRoute('user.login', ['user' => 'werknemer', 'mail' => $values['mail']])->toString());
    $response->send();
  }
}
