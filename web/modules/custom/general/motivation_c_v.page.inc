<?php

/**
 * @file
 * Contains motivation_c_v.page.inc.
 *
 * Page callback for Motivation cv entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Motivation cv templates.
 *
 * Default template: motivation_c_v.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_motivation_c_v(array &$variables) {
  // Fetch MotivationCV Entity Object.
  $motivation_c_v = $variables['elements']['#motivation_c_v'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
