<?php

namespace Drupal\general\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\general\Entity\Value;
use Drupal\general\Plugin\QueryHelper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Class UserEmployeRegisterFormStep1.
 */
class UserEmployeeRegisterFormStep1 extends FormBase
{

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * Class constructor.
   * @param AccountInterface $account
   * @param PrivateTempStoreFactory $temp_store_factory
   * @param SessionManagerInterface $session_manager
   * @param ConfigFactory $configFactory
   * @param LanguageManagerInterface $languageManager
   * @param QueryHelper $queryHelper
   */
  public function __construct(AccountInterface $account, PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, ConfigFactory $configFactory, LanguageManagerInterface $languageManager, QueryHelper $queryHelper)
  {
    $this->account = $account;
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->store = $this->tempStoreFactory->get('multistep_data_company_register');
    $this->configFactory = $configFactory;
    $this->languageManager = $languageManager;
    $this->queryHelper = $queryHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('user.private_tempstore'),
      $container->get('session_manager'),
      $container->get('config.factory'),
      $container->get('language_manager'),
      $container->get('general.query_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'user_employee_form_step_1';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($this->account->id() !== 0) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    // Start a manual session for anonymous users.
    if (!isset($_SESSION['multistep_form_holds_session'])) {
      $_SESSION['multistep_form_holds_session'] = true;
      $this->sessionManager->start();
    }

    // Basic values.
    $form['basic_values'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['basic_values', 'horizontal-tabs']],
    ];
    $form['basic_values']['basic_values_tabs'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['horizontal-tabs-list']],
    ];
    $form['basic_values']['basic_values_tabs']['title_1'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '1.' . $this->t('Basis gegevens'),
      '#attributes' => ['class' => ['selected', 'horizontal-tab-button']]
    ];
    $form['basic_values']['basic_values_tabs']['title_2'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '2.' . $this->t('Persoonlijke gegevens'),
      '#attributes' => ['class' => ['horizontal-tab-button']]
    ];
    $form['basic_values']['basic_values_tabs']['title_3'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '3.' . $this->t('Waarden'),
      '#attributes' => ['class' => ['horizontal-tab-button']]
    ];
    $form['basic_values']['emp_firstname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Voornaam'),
      '#required' => TRUE,
    ];
    $form['basic_values']['emp_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Naam'),
      '#required' => TRUE,
    ];
    $form['basic_values']['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mailadres'),
      '#required' => TRUE,
    ];
    $form['basic_values']['password'] = [
      '#type' => 'password_confirm',
      '#required' => TRUE,
    ];

    $alias = $this->queryHelper->getAliasByPath('/node/4');
    $form['basic_values']['privacy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ik ga akkoord met de <a target="_blank" href=":link">privacyverklaring</a>', [':link' => $alias]),
      '#required' => TRUE,
    ];

    $form['basic_values']['required_info'] = [
      '#markup' => '<p><span class="form-required"></span>' . $this->t('Verplicht in te vullen') . '</p>',
    ];

    $form['basic_values']['button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Volgende'),
      '#attributes' => ['class' => ['button-primary']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
    $user = user_load_by_mail($form_state->getValue('mail'));
    if ($user !== FALSE) {
      $form_state->setErrorByName('mail', t('Er bestaat al een gebruiker met dit e-mailadres'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $fields = [];
    foreach ($form_state->getValues() as $key => $value) {
      $this->store->set($key, $value);
      $fields[] = $key;
    }
    $this->store->set('fields', $fields);
    $form_state->setRedirect('general.user_employee_register_form_step2');
  }
}
