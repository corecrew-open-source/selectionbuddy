<?php

namespace Drupal\context_translation\TwigExtension;

use Drupal\context_translation\ContextStringTranslationTrait;


/**
 * Class ContextTranslationTwigExtension.
 */
class ContextTranslationTwigExtension extends \Twig_Extension {

  use ContextStringTranslationTrait;

  /**
    * {@inheritdoc}
    */
    public function getFilters() {
      return [
        new \Twig_SimpleFilter('t_ct', [$this, 'tCt'], ['is_safe' => ['html']]),
        new \Twig_SimpleFilter('trans_ct', [$this, 'tCt'], ['is_safe' => ['html']]),
      ];
    }


  /**
   * Translates the string with a custom context.
   *
   * @param mixed $input
   *
   * @return string
   */
  public function tCt($input) {
    if (!is_string($input)) {
      $input = render($input);
    }

    return $this->t_ct($input);
  }

   /**
    * {@inheritdoc}
    */
    public function getName() {
      return 'context_translation.twig.extension';
    }

}
