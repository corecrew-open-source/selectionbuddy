<?php

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function general_help($route_name, RouteMatchInterface $route_match)
{
  switch ($route_name) {
    // Main module help for the general module.
    case 'help.page.general':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('A general module for the custom functionality') . '</p>';
      return $output;

    default:
  }
}
