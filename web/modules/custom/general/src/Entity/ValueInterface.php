<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Value entities.
 *
 * @ingroup general
 */
interface ValueInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Value name.
   *
   * @return string
   *   Name of the Value.
   */
  public function getName();

  /**
   * Sets the Value name.
   *
   * @param string $name
   *   The Value name.
   *
   * @return \Drupal\general\Entity\ValueInterface
   *   The called Value entity.
   */
  public function setName($name);

  /**
   * Gets the Value creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Value.
   */
  public function getCreatedTime();

  /**
   * Sets the Value creation timestamp.
   *
   * @param int $timestamp
   *   The Value creation timestamp.
   *
   * @return \Drupal\general\Entity\ValueInterface
   *   The called Value entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Value published status indicator.
   *
   * Unpublished Value are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Value is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Value.
   *
   * @param bool $published
   *   TRUE to set this Value to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\general\Entity\ValueInterface
   *   The called Value entity.
   */
  public function setPublished($published);

}
