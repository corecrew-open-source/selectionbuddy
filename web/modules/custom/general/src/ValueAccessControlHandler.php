<?php

namespace Drupal\general;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Value entity.
 *
 * @see \Drupal\general\Entity\Value.
 */
class ValueAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\general\Entity\ValueInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished value entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published value entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit value entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete value entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add value entities');
  }

}
