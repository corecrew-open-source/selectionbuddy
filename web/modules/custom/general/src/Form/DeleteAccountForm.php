<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\general\Entity\Application;
use Drupal\general\Entity\DefaultStep;
use Drupal\general\Entity\Job;
use Drupal\general\Entity\MotivationCV;
use Drupal\general\Entity\PersoResult;
use Drupal\general\Entity\SJTresult;
use Drupal\general\Entity\Value;
use Drupal\general\Plugin\QueryHelper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DeleteAccountForm.
 */
class DeleteAccountForm extends FormBase
{

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var AccountProxy
   */
  protected $currentUser;

  /**
   * Class constructor.
   * @param QueryHelper $queryHelper
   */
  public function __construct(QueryHelper $queryHelper, AccountProxy $currentUser)
  {
    $this->queryHelper = $queryHelper;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('general.query_helper'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'delete_profile_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form_state->disableCache();
    $user = User::load($this->currentUser->id());
    if (!in_array('employe', $user->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    $form['delete_group']['delete_text'] = [
      '#markup' => '<strong>' . t('Wilt u uw profiel verwijderen?') . '</strong><p>' . t('Deze actie is onomkeerbaar.') . '</p>',
    ];

    $form['buttons']['annuleer'] = [
      '#type' => 'submit',
      '#value' => $this->t('Annuleer'),
      '#submit' => ['::cancelForm'],
      '#limit_validation_errors' => [],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Verwijderen'),
      '#submit' => array('::submitForm'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state)
  {
    $form_state->setRedirect('general.user_controller_getEmployeeProfilePage');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // Get all the applications.
    $applicationIds = $this->queryHelper->getDataToDeleteUser('application');

    // Get all the steps linked to these applications.
    $applications = Application::loadMultiple($applicationIds);
    foreach ($applications as $application) {
      $steps = $application->get('field_app_all_steps')->getValue()[0]['value'];
      $steps = json_decode($steps, TRUE);
      foreach ($steps as $step) {
        if ($step['type'] === 'motivation_c_v' ) {
          $stepEntity = MotivationCV::load($step['id']);
          $stepEntity->delete();
        }
        if($step['type'] === 'default_step') {
          $stepEntity = DefaultStep::load($step['id']);
          $stepEntity->delete();
        }
      }

      // Remove the applications
      $application->delete();
    }

    // Remove the sjt_results for this application.
    $sjtResults = $this->queryHelper->getDataToDeleteUser('sjt_result');
    $sjtResultsEntities = SJTresult::loadMultiple($sjtResults);
    foreach ($sjtResultsEntities as $result) {
      $result->delete();
    }

    // Remove all the perso_result for this application
    $persoResults = $this->queryHelper->getDataToDeleteUser('perso_result');
    $persoResultsEntities = PersoResult::loadMultiple($persoResults);
    foreach ($persoResultsEntities as $result) {
      $result->delete();
    }

    // Remove the "Waarden" for the user.
    $userValues = $this->queryHelper->getDataToDeleteUser('value');
    $userValuesEntities = PersoResult::loadMultiple($userValues);
    foreach ($userValuesEntities as $result) {
      $result->delete();
    }

    // Delete the user.
    $user = User::load($this->currentUser->id());
    $user->delete();

    // Logout the current user.
    user_logout();
  }
}
