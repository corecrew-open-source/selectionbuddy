<?php

namespace Drupal\general\Form;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\general\Entity\Job;
use Drupal\general\Entity\PersoResult;
use Drupal\general\Entity\SJTresult;
use Drupal\general\Plugin\QueryHelper;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class JobApplyForm.
 */
class SjtCreateForm extends FormBase
{
  use ContextStringTranslationTrait;

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var MailManager
   */
  protected $mailManager;

  /**
   * Class constructor.
   * @param AccountInterface $account
   * @param LanguageManagerInterface $languageManager
   * @param QueryHelper $queryHelper
   * @param EntityRepositoryInterface $entityRepository
   * @param RequestStack $requestStack
   * @param MailManager $mailManager
   */
  public function __construct(AccountInterface $account, LanguageManagerInterface $languageManager, QueryHelper $queryHelper, EntityRepositoryInterface $entityRepository, RequestStack $requestStack,  MailManager $mailManager)
  {
    $this->account = $account;
    $this->languageManager = $languageManager;
    $this->queryHelper = $queryHelper;
    $this->entityRepository = $entityRepository;
    $this->request = $requestStack->getCurrentRequest();
    $this->mailManager = $mailManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('general.query_helper'),
      $container->get('entity.repository'),
      $container->get('request_stack'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'sjt_create_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form_state->disableCache();
    if ($this->account->id() === 0 || !in_array('employe', $this->account->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    $jobId = $this->request->get('id');
    if ($jobId !== NULL) {
      $jobEnityId = $this->queryHelper->getJobByUniqueId($jobId);
      $job = Job::load(reset($jobEnityId));
      $form['job_id'] = [
        '#type' => 'hidden',
        '#value' => reset($jobEnityId),
      ];

      $form['sjt'] = [
        '#type' => 'hidden',
        '#value' => $job->field_job_sjt->value,
      ];

      $form['perso'] = [
        '#type' => 'hidden',
        '#value' => $job->field_job_personalities->value,
      ];

      if ($job->field_job_sjt->value === '1') {
        $categoryIds = array_column($job->field_job_sjt_categories->getValue(), 'target_id');
        $form['intro'] = [
          '#markup' => '<h3>' . $this->t_ct('Competentievragen') . '</h3><p>' . $this->t_ct('Hieronder worden enkele situaties geschetst en er wordt gevraagd hoe je zou reageren. Duid de meest waarschijnlijke reactie aan met een groen vinkje en de minst waarschijnlijke met een rood kruisje.') . '</p>',
        ];

        $questions = $this->queryHelper->getTaxonomyFormArr('sjt_questions', 'raw');
        $questionsHidden = [];
        foreach ($questions as $question) {
          $options = [];
          $optionsSelect = [];
          if (count($categoryIds) > 0 && in_array($question->field_quest_category->getValue()[0]['target_id'], $categoryIds)) {
            $form['question_' . $question->id() . '_container'] = [
              '#type' => 'container',
              '#attributes' => ['class' => ['question-' . $question->id() . '-container', 'question-container']],
            ];
            for ($j = 1; $j <= 2; $j++) {
              for ($i = 1; $i <= 4; $i++) {
                $answer = [
                  'label' => $question->get('field_quest_answer_' . $i)->getValue()[0]['value'],
                  'value' => (int)$question->get('field_quest_answer_' . $i . '_value')->getValue()[0]['value'],
                ];
                if ($j === 2) {
                  $answer['value'] = $answer['value'] * -1;
                }
                $options[$i] = $answer;
                $optionsSelect[$i] = $answer['label'];
              }

              $form['forma_' . $question->id() . '_' . $j . '_answers'] = [
                '#type' => 'hidden',
                '#value' => json_encode($options),
              ];
              $form['question_' . $question->id() . '_container']['forma_' . $question->id() . '_' . $j] = [
                '#type' => 'radios',
                '#required' => TRUE,
                '#title' => $question->field_quest_question->getValue()[0]['value'],
                '#options' => $optionsSelect,
                '#atrributes' => ['class' => 'sjt-question-' . $question->id()],
                '#required_error' => $this->t_ct('Dit is een verplicht veld')
              ];
              $questionsHidden[$question->id() . '_' . $j] = [
                'label' => $form['question_' . $question->id() . '_container']['forma_' . $question->id() . '_1']['#title'],
                'category' => $question->field_quest_category->referencedEntities()[0]->label(),
              ];
            }
          }
        }

        $form['form_questions'] = [
          '#type' => 'hidden',
          '#value' => json_encode($questionsHidden),
        ];
      }

      // Add the legend to the side of the screen.
      $legendArr = [
        1 => $this->t('Helemaal niet akkoord'),
        2 => $this->t('Niet akkoord'),
        3 => $this->t('Neutraal'),
        4 => $this->t('Akkoord'),
        5 => $this->t('Helemaal akkoord'),
      ];
      $buildLegend = '';
      foreach ($legendArr as $key => $element) {
        $buildLegend .= '<li class="star-' . $key . '">';
        $buildLegend .= $element;
        $buildLegend .= '</li>';
      }

      if ($job->field_job_personalities->value === '1') {
        $form['intro_perso'] = [
          '#markup' => '<h3>' . $this->t('Persoonlijkheidsvragen') . '</h3><p>' . $this->t('Geef hieronder aan hoe je jezelf ziet op het werk') . '</p>',
        ];

        $form['legend'] = [
          '#markup' => '<ul class="legend">'. $buildLegend .'</ul>',
        ];

        $questions = $this->queryHelper->getTaxonomyFormArr('perso_question', 'raw');
        $questionsHidden = [];
        foreach ($questions as $question) {
          $values = [];
          $valuesInvers = [];
          $nuberAnswers = 5;
          for ($i = 1; $i <= $nuberAnswers; $i++) {
            $values[$i] = $i;
          }
          for ($i = $nuberAnswers; $i >= 1; $i--) {
            $valuesInvers[$i] = abs($i - ($nuberAnswers + 1));
          }

          $form['formb_' . $question->id()] = [
            '#type' => 'radios',
            '#required' => TRUE,
            '#title' => $question->label(),
            '#options' => $question->field_perso_invers->value === '1' ? $valuesInvers : $values,
            '#attributes' => ['class' => ['personality-question-container']],
            '#required_error' => $this->t_ct('Dit is een verplicht veld')
          ];
          $questionsHidden[$question->id()] = [
            'label' => $form['formb_' . $question->id()]['#title'],
            'category' => $question->field_perso_category->referencedEntities()[0]->label(),
          ];
        }
        $form['formb_questions'] = [
          '#type' => 'hidden',
          '#value' => json_encode($questionsHidden),
        ];
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Verstuur'),
      '#submit' => array('::submitForm'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    // Get the current application by the jobId and the userid.
    $jobEntity = Job::load($values['job_id']);

    // Create the application for the current user linked to correct job.
    // Check if the user has already an application for the current job.
    $jobApplications = $jobEntity->field_job_applications->referencedEntities();
    if (count($jobApplications) > 0) {
      foreach ($jobApplications as $application) {
        $applicationUser = $application->get('user_id')->getValue()[0]['target_id'];
        if ($applicationUser == $this->currentUser()->id()) {
          if ($values['sjt'] === '1') {
            $questions = json_decode($values['form_questions'], TRUE);
            $data = [];
            $count = 0;
          }
          if ($values['perso'] === '1') {
            $dataPerso = [];
            $questionsPerso = json_decode($values['formb_questions'], TRUE);
          }

          foreach ($values as $id => $value) {
            if (strpos($id, 'forma_') !== FALSE && strpos($id, '_answers') === FALSE && $values['sjt'] === '1') {
              $count++;
              $questionId = str_replace('forma_', '', $id);
              $answers = (array)json_decode($values['forma_' . $questionId . '_answers'], TRUE);
              if (!isset($data[$questions[$questionId]['category']])) {
                $data[$questions[$questionId]['category']] = [
                  'score' => (int)$answers[$values[$id]]['value'],
                  'questions' => [],
                ];
              } else {
                $data[$questions[$questionId]['category']]['score'] = $data[$questions[$questionId]['category']]['score'] + (int)$answers[$values[$id]]['value'];
              }
              $data[$questions[$questionId]['category']]['questions'][] = [
                'id' => $questionId,
                'label' => $questions[$questionId]['label'],
                'answer' => $answers[$values[$id]],
              ];
            }

            if (strpos($id, 'formb_') !== FALSE && strpos($id, 'formb_questions') === FALSE && $values['perso'] === '1') {
              $questionId = str_replace('formb_', '', $id);
              if (!isset($dataPerso[$questionsPerso[$questionId]['category']])) {
                $dataPerso[$questionsPerso[$questionId]['category']] = [
                  'score' => (int) $value,
                ];
              } else {
                $dataPerso[$questionsPerso[$questionId]['category']]['score'] = $dataPerso[$questionsPerso[$questionId]['category']]['score'] + (int) $value;
              }
            }
          }

          $allSteps = json_decode($application->field_app_all_steps->value, TRUE);
          if ($values['sjt'] === '1') {
            foreach ($data as &$category) {
              $category['score'] = $category['score'] / (count($category['questions']) / 2);
            }

            $newStep = SJTresult::create([
              'status' => 1,
              'uid' => $this->account->id(),
              'field_sjt_application' => ['target_id' => $application->id()],
              'field_sjt_data' => json_encode($data),
            ]);
            $newStep->save();

            $allSteps[$application->field_app_current_step_index->value]['tests'] = [[
              'type' => 'sjt',
              'id' => $newStep->id(),
            ]];
          }

          if($values['perso'] === '1') {
            $newStep = PersoResult::create([
              'status' => 1,
              'uid' => $this->account->id(),
              'field_perso_application' => ['target_id' => $application->id()],
              'field_perso_data' => json_encode($dataPerso),
            ]);
            $newStep->save();

            if ($values['sjt'] === '1') {
              $allSteps[$application->field_app_current_step_index->value]['tests'][] = [
                'type' => 'perso',
                'id' => $newStep->id(),
              ];
            } else {
              $allSteps[$application->field_app_current_step_index->value]['tests'] = [[
                'type' => 'perso',
                'id' => $newStep->id(),
              ]];
            }
          }

          $application->set('field_app_all_steps', json_encode($allSteps));
          $application->save();

          $user = User::load($this->currentUser()->id());
          $name = $user->get('field_emp_firstname')->getValue()[0]['value'] . ' ' . $user->get('field_emp_name')->getValue()[0]['value'];
          $this->sendMail($jobEntity->getOwnerId(), $jobEntity->label(), $name, $application->id());

          $current_uri = $this->request->getRequestUri();
          $form_state->setRedirectUrl(Url::fromUserInput($current_uri));
        }
      }
    }
  }

  private function sendMail($companyUserId, $job, $applicationUser, $applicationId) {
    $company = User::load($companyUserId);
    if (isset($company->get('field_mails')->getValue()[0]) && $company->get('field_mails')->getValue()[0]['value'] === '1') {
      $to = $company->getEmail();

      // Message.
      $params['message'] = t('Beste, <br />@user heeft zonet de vragen afgewerkt voor jullie vacature van @job. <br />Je kan de resultaten <a href="/applications/job-detail/@jobId">hier</a> bekijken.', ['@job' => $job, '@user' => $applicationUser, '@jobId' => $applicationId]);
      $params['subject'] = t('@user heeft zonet de testen afgewerkt voor @job', ['@job' => $job, '@user' => $applicationUser]);

      // Send mail to employee.
      $result = $this->mailManager->mail('general', 'tests_finished', $to, 'nl', $params, NULL, TRUE);
      return $result['result'];
    }

    return FALSE;
  }
}
