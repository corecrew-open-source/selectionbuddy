<?php

namespace Drupal\general;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for value.
 */
class ValueTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
