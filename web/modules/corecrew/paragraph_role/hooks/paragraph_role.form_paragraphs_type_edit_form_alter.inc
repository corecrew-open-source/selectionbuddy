<?php

use Drupal\Core\Form\FormStateInterface;
use \Drupal\paragraphs\ParagraphsTypeInterface;
use \Drupal\field\Entity\FieldConfig;
use \Drupal\field\Entity\FieldStorageConfig;
use \Drupal\paragraphs\Entity\Paragraph;

/**
 * Implements hook_form_FORM_ID_alter.
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function paragraph_role_form_paragraphs_type_edit_form_alter(array &$form, FormStateInterface $form_state)
{
  // Retrieve the entity to be able to store data.
  $type = $form_state->getFormObject()->getEntity();

  // A new tab needs to be created to avoid issues with the default ajax calls on the menu settings tab.
  if (!isset($form['paragraph_role_options'])) {
    $form['paragraph_role_options'] = [
      '#type' => 'details',
      '#title' => t('Paragraph roles'),
      '#group' => 'additional_settings',
    ];
  }

  // Add a checkbox to the existing form.
  $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
  $options = [];
  foreach ($roles as $key => $role) {
    $options[$key] = $role->label();
  }
  $form['paragraph_role_options']['paragraph_roles'] = [
    '#type' => 'checkboxes',
    '#options' => $options,
    '#title' => t('Available roles'),
    '#description' => t('Enable the roles that can see this paragraph. Leave empty for all roles'),
    '#default_value' => $type->getThirdPartySetting('paragraph_role', 'paragraph_roles', FALSE),
  ];

  // Add a submit handler to store the custom checkbox.
  $form['#entity_builders'][] = '_paragraph_role_form_paragraphs_type_edit_form_builder';
}

/**
 * Entity builder for the paragraph type form with scheduler options.
 *
 * @param $entity_type
 * @param ParagraphsTypeInterface $type
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function _paragraph_role_form_paragraphs_type_edit_form_builder($entity_type, ParagraphsTypeInterface $type, &$form, FormStateInterface $form_state)
{
  // Get the settings from the form_state, build the fieldname.
  $newSettings = $form_state->getValue('paragraph_roles');
  $bundle = $type->id();
  $fieldName = 'field_' . $bundle . '_roles';

  // Save the settings.
  $type->setThirdPartySetting(
    'paragraph_role',
    'paragraph_roles',
    $newSettings
  );

  // Build the options.
  $roles = [];
  foreach ($newSettings as $key => $role) {
    if ($role !== 0) {
      $roles[$key] = ucfirst($key);
    }
  }

  if (!empty($roles)) {
    // Make label & description.
    $label = t('Available roles');
    $description = t('The users who have one of the selected roles can see this block.');
    $entityTypemanager = \Drupal::entityTypeManager();

    // Check if there is allready a fieldStorageConfig .
    if (empty($fieldStorageRolesField = FieldStorageConfig::loadByName('paragraph', $fieldName))) {
      // Create & save the fieldStorage.
      $fieldStorage = FieldStorageConfig::create(
        [
          'field_name' => $fieldName,
          'langcode' => 'en',
          'entity_type' => 'paragraph',
          'type' => 'entity_reference',
          'settings' => [
            'target_type' => 'user_role',
          ],
          'module' => 'options',
          'locked' => FALSE,
          'cardinality' => -1,
          'translatable' => TRUE,
          'persist_with_no_fields' => FALSE,
          'custom_storage' => FALSE,
        ]
      );
      $fieldStorage->save();
    } else {
      // If you remove a role from the settings they have to be removed from the paragraphs using it.
      $query = $entityTypemanager->getStorage('paragraph')->getQuery();
      $query->condition('type', $bundle);
      $data = $query->execute();
      $paras = Paragraph::loadMultiple($data);

      foreach ($paras as $para) {
        $roles = array_column($para->get($fieldName)->getValue(), 'target_id');
        $newRoles = array_intersect($roles, $newSettings);
        $value = [];
        foreach ($newRoles as $key => $newRole) {
          $value[$key] = ['target_id' => $newRole];
        }
        $para->set($fieldName, $value);
        $para->save();
      }
    }

    // Create & save the field is not exists.
    $field = FieldConfig::loadByName('paragraph', $bundle, $fieldName);
    if (empty($field)) {
      FieldConfig::create(
        [
          'field_name' => $fieldName,
          'entity_type' => 'paragraph',
          'bundle' => $bundle,
          'label' => $label,
          'translatable' => TRUE,
          'description' => $description,
        ]
      )->save();

      // Assign widget settings for the 'default' form mode.
      $displayForm = $entityTypemanager
        ->getStorage('entity_form_display')
        ->load('paragraph.' . $bundle . '.default')
        ->setComponent($fieldName, [
          'type' => 'options_buttons',
          'weight' => 100,
        ]);

      $displayForm->save();
      unset($displayForm);
    }
  } else {
    // Remove the field if there are no roles selected.
    $entityFieldManager = Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions('paragraph', $bundle);
    if (isset($fields[$fieldName])) {
      $fields[$fieldName]->delete();
    }
  }

}
