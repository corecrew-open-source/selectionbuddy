<?php

namespace Drupal\general;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Default step entity.
 *
 * @see \Drupal\general\Entity\DefaultStep.
 */
class DefaultStepAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\general\Entity\DefaultStepInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished default step entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published default step entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit default step entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete default step entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add default step entities');
  }

}
