<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\general\Entity\Value;
use Drupal\general\Plugin\QueryHelper;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class UserController.
 */
class UserController extends ControllerBase {

  /**
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var string
   */
  protected $currentLanguage;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  public function __construct(AccountProxyInterface $currentUser, EntityTypeManagerInterface $entityTypeManager, QueryHelper $queryHelper, LanguageManagerInterface $languageManager, EntityRepositoryInterface $entityRepository)
  {
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->queryHelper = $queryHelper;
    $this->languageManager = $languageManager;
    $this->entityRepository = $entityRepository;
    $this->currentLanguage = $languageManager->getCurrentLanguage()->getId();
  }

  /**
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    $currentUser = $container->get('current_user');
    $typemanager = $container->get('entity_type.manager');
    $queryHelper = $container->get('general.query_helper');
    $languagemanager = $container->get('language_manager');
    $entityRepository = $container->get('entity.repository');
    return new static($currentUser, $typemanager, $queryHelper, $languagemanager, $entityRepository);
  }

  /**
   * Getcompanyprofilepage.
   */
  public function getCompanyProfilePage() {
    if (!in_array('company', $this->currentUser->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }
    $user = User::load($this->currentUser->id());

    // Get the logo
    $image = NULL;
    if (isset($user->get('field_company_logo')->getValue()[0])) {
      $logoFid = $user->get('field_company_logo')->getValue()[0]['target_id'];
      if ($logoFid !== NULL) {
        $style = $this->entityTypeManager->getStorage('image_style')->load('logo');
        $image = $style->buildUrl(File::load($logoFid)->getFileUri());
      }
    }
    // Get the slogan
    $slogan = NULL;
    if (isset($user->get('field_company_slogan')->getValue()[0])) {
      $slogan = $user->get('field_company_slogan')->getValue()[0]['value'];
    }

    // Fields for the sidebar
    $fields = [
      'adres' => '',
      'sector' => '',
      'kernactiviteit' => '',
      'contactpersoon' => '',
      'e-mailadres contactpersoon' => '',
      'website' => '',
    ];
    if (isset($user->get('field_company_street_number')->getValue()[0]) && isset($user->get('field_company_postalcode_state')->getValue()[0])) {
      $fields['adres'] = $user->get('field_company_street_number')->getValue()[0]['value'] . ' ' . $user->get('field_company_postalcode_state')->getValue()[0]['value'];
    }
    if (isset($user->get('field_company_sector')->getValue()[0])) {
      $fields['sector'] = $user->get('field_company_sector')->getValue()[0]['value'];
    }
    if (isset($user->get('field_company_core_activity')->getValue()[0])) {
      $fields['kernactiviteit'] = $user->get('field_company_core_activity')->getValue()[0]['value'];
    }
    if (isset( $user->get('field_company_contactperson')->getValue()[0])) {
      $fields['contactpersoon'] = $user->get('field_company_contactperson')->getValue()[0]['value'];
    }
    $fields['e-mailadres contactpersoon'] = $user->getEmail();
    if (isset($user->get('field_company_website')->getValue()[0])) {
      $fields['website'] = $user->get('field_company_website')->getValue()[0]['value'];
    }

    // Get the values
    $valueIds = $this->queryHelper->getValuesForCurrentUser();
    $values = Value::loadMultiple($valueIds);
    $valuesReturn = [];
    foreach ($values as $value) {
      if($value->hasTranslation($this->currentLanguage) ) {
        $translatedValue = $this->entityRepository->getTranslationFromContext($value, $this->currentLanguage);
        $term = $translatedValue->field_value_term->referencedEntities();
        $translatedTerm = $this->entityRepository->getTranslationFromContext($term[0], $this->currentLanguage);
        $valuesReturn[] = [
          'name' => $translatedTerm->label(),
          'rating' => $translatedValue->get('field_value_rating')->getValue()[0]['value'],
        ];
      }
    }

    return [
      '#theme' => 'company_profile_page',
      '#company' => $user->get('field_company_company')->getValue()[0]['value'],
      '#logo' => $image,
      '#slogan' => $slogan,
      '#fields' => $fields,
      '#values' => $valuesReturn,
    ];
  }

  /**
   * Getcompanyprofilepage.
   */
  public function getCompanyProfilePageTitle()
  {
    if (!in_array('company', $this->currentUser->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }
    $user = User::load($this->currentUser->id());

    return $user->get('field_company_company')->getValue()[0]['value'];
  }

    /**
   * redirectToProfile.
   */
  public function redirectToProfile() {
    $user = User::load($this->currentUser->id());
    if ($user->isAnonymous()) {
      $url = Url::fromRoute('user.login');
      return new RedirectResponse($url->toString());
    }
    if (in_array('company', $user->getRoles())) {
      $url = Url::fromRoute('general.user_company_edit_form');
    } else if (in_array('employe', $user->getRoles())) {
      $url = Url::fromRoute('general.user_employee_edit_form');
    }

    return new RedirectResponse($url->toString());
  }


  public function getEmployeeProfilePage() {
    if (!in_array('employe', $this->currentUser->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }
    $user = User::load($this->currentUser->id());

    // Get the profile picture
    $image = NULL;
    if (isset($user->get('field_emp_picture')->getValue()[0])) {
      $logoFid = $user->get('field_emp_picture')->getValue()[0]['target_id'];
      if ($logoFid !== NULL) {
        $style = $this->entityTypeManager->getStorage('image_style')->load('picture');
        $image = $style->buildUrl(File::load($logoFid)->getFileUri());
      }
    }

    // Get the age
    $age = NULL;
    if (isset($user->get('field_emp_birth')->getValue()[0])) {
      $age = $user->get('field_emp_birth')->getValue()[0]['value'];
    }
    $age = date_diff(date_create($age), date_create('now'))->y;
    $age .= t(' jaar')->__toString();

    // Get the gender
    $gender = NULL;
    if (isset($user->get('field_emp_sex')->getValue()[0])) {
      $gender = $user->get('field_emp_sex')->getValue()[0]['value'];
    }

    $name = $user->get('field_emp_firstname')->getValue()[0]['value'] . ' ' . $user->get('field_emp_name')->getValue()[0]['value'];

    // Fields for the sidebar
    $fields = [
      'geboortedatum *' => '',
      'diploma' => '',
      'telefoonnummer *' => '',
    ];
    if (isset($user->get('field_emp_birth')->getValue()[0]) && isset($user->get('field_emp_birth')->getValue()[0])) {
      $fields['geboortedatum *'] = (new \DateTime($user->get('field_emp_birth')->getValue()[0]['value']))->format('d m Y');
    }
    if (isset($user->get('field_emp_diploma')->getValue()[0])) {
      $fields['diploma'] = $user->get('field_emp_diploma')->getValue()[0]['value'];
    }
    if (isset($user->get('field_emp_phone')->getValue()[0])) {
      $fields['telefoonnummer *'] = $user->get('field_emp_phone')->getValue()[0]['value'];
    }
    $fields['e-mailadres'] = $user->getEmail();

    // Get the values
    $valueIds = $this->queryHelper->getValuesForCurrentUser();
    $values = Value::loadMultiple($valueIds);
    $valuesReturn = [];
    foreach ($values as $value) {
      if($value->hasTranslation($this->currentLanguage) ) {
        $translatedValue = $this->entityRepository->getTranslationFromContext($value, $this->currentLanguage);
        $term = $translatedValue->field_value_term->referencedEntities();
        $translatedTerm = $this->entityRepository->getTranslationFromContext($term[0], $this->currentLanguage);
        $valuesReturn[] = [
          'name' => $translatedTerm->label(),
          'rating' => $translatedValue->get('field_value_rating')->getValue()[0]['value'],
        ];
      }
    }

    return [
      '#theme' => 'employee_profile_page',
      '#picture' => $image,
      '#age' => $age,
      '#gender' => $gender,
      '#fields' => $fields,
      '#values' => $valuesReturn,
      '#name' => $name,
    ];
  }

}
