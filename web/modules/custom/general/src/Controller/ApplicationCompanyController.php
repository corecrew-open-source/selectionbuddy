<?php

namespace Drupal\general\Controller;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\general\Entity\Application;
use Drupal\general\Entity\Job;
use Drupal\general\Entity\PersoResult;
use Drupal\general\Entity\SJTresult;
use Drupal\general\Entity\Value;
use Drupal\general\Plugin\QueryHelper;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ApplicationCompanyController.
 */
class ApplicationCompanyController extends ControllerBase
{
  use ContextStringTranslationTrait;

  /**
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var FormBuilder
   */
  protected $formBuilder;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var string
   */
  protected $currentLanguage;

  /**
   * @var EntityFormBuilder
   */
  protected $entityFormBuilder;

  /**
   * ApplicationController constructor.
   * @param AccountProxyInterface $currentUser
   * @param QueryHelper $queryHelper
   * @param EntityTypeManager $entityTypeManager
   * @param FormBuilder $formBuilder
   * @param EntityRepositoryInterface $entityRepository
   * @param LanguageManagerInterface $languageManager
   * @param EntityFormBuilder $entityFormBuilder
   */
  public function __construct(AccountProxyInterface $currentUser, QueryHelper $queryHelper, EntityTypeManager $entityTypeManager, FormBuilder $formBuilder, EntityRepositoryInterface $entityRepository, LanguageManagerInterface $languageManager, EntityFormBuilder $entityFormBuilder)
  {
    $this->queryHelper = $queryHelper;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
    $this->entityRepository = $entityRepository;
    $this->languageManager = $languageManager;
    $this->currentLanguage = $languageManager->getCurrentLanguage()->getId();
    $this->entityFormBuilder = $entityFormBuilder;
  }

  /**
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('current_user'),
      $container->get('general.query_helper'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('entity.form_builder')
    );
  }

  /**
   * Getapplicationdetails.
   */
  public function getApplicationDetail($id = NULL)
  {
    // Check if id isset and user has role company.
    if ($id === NULL && in_array('company', $this->currentUser->getRoles()) !== FALSE) {
      return new RedirectResponse(Url::fromRoute('<front>'));
    }

    // Get the first step.
    $step = $this->queryHelper->getStepsByIndexAndApplicationId(1, $id);

    // Get the current step index
    $application = Application::load($id);
    $currentStepIndex = (int)$application->field_app_current_step_index->value + 1;
    $tabs = $this->queryHelper->getTabsforCurrentJobCompany($application);

    // CV.
    $cv = NULL;
    if (isset($step->field_step1_cv->referencedEntities()[0]) && !$this->queryHelper->isJobAnon($application->id())) {
      $cv = file_create_url($step->field_step1_cv->referencedEntities()[0]->getFileUri());
    }

    $steps = json_decode($application->field_app_all_steps->getValue()[0]['value'], TRUE);
    $employeeIsTesting = FALSE;
    $forms = [];
    for ($i = 1; $i < count($steps); $i++) {
      if ($steps[$i]['type'] !== 'tests') {
        $forms[] = [
          'label' => $steps[$i]['label'],
          'form' => $this->getFormOrRender($application->id(), $i, $currentStepIndex),
          'active' => ($i + 1) > $currentStepIndex,
          'current' => ($i + 1) == $currentStepIndex,
          'type' => 'step',
        ];
      } else {
        $bool = TRUE;
        foreach ($steps[$i]['tests'] as $test) {
          if (is_array($test)) {
            $data = [];
            if ($test['type'] === 'sjt') {
              $sjt = [];
              $sjtEntity = SJTresult::load($test['id']);
              $sjt['data'] = json_decode($sjtEntity->get('field_sjt_data')->getValue()[0]['value'], TRUE);
              $data['sjt'] = $sjt;
            }
            if ($test['type'] === 'perso') {
              $perso = [];
              $persoEntity = PersoResult::load($test['id']);
              $perso['data'] = json_decode($persoEntity->get('field_perso_data')->getValue()[0]['value'], TRUE);
              $data['perso'] = $perso;
            }
            $forms[] = [
              'label' => 'Testen',
              'form' => $data,
              'active' => ($i + 1) > $currentStepIndex,
              'current' => ($i + 1) == $currentStepIndex,
              'type' => 'tests',
            ];
          } else {
            if (!$employeeIsTesting && ($i + 1) == $currentStepIndex) {
              $employeeIsTesting = TRUE;
            }
            if ($bool) {
              $forms[] = [
                'label' => $this->t_ct('De sollicitant dient de test nog in te vullen.')->__toString(),
                'form' => ['#markup' => ' '],
                'active' => ($i + 1) > $currentStepIndex,
                'current' => ($i + 1) == $currentStepIndex,
                'type' => 'step',
              ];
            }
            if (count($steps[$i]['tests']) > 1) {
              $bool = FALSE;
            }
          }
        }
      }
    }

    $ratings = [];
    for ($i = 1; $i <= 5; $i++) {
      if (isset($application->get('field_step_' . $i . '_rating')->getValue()[0])) {
        $ratings[$i] = $application->get('field_step_' . $i . '_rating')->getValue()[0]['value'];
      }
    }

    $lastStep = FALSE;
    if ($currentStepIndex === count($steps)) {
      $lastStep = TRUE;
    }

    $jobIds = $this->queryHelper->getJobByApplicationId($application->id());

    $returnArr = [
      '#theme' => 'application_company',
      '#title' => '',
      '#job_id' => reset($jobIds),
      '#profileInfo' => $this->getProfileValues($application),
      '#changed' => $this->t_ct('laatste wijziging op') . ' ' . (new \DateTime('@' . $application->getChangedTime()))->format('d m Y'),
      '#tabs' => $tabs,
      '#stepFields' => $this->getStepFields($step, $application->id()),
      '#stepExtraFields' => $this->getStepExtraFields($step),
      '#cv' => $cv,
      '#id' => $id,
      '#currentStepIndex' => $currentStepIndex,
      '#forms' => $forms,
      '#valueCompare' => $this->getCompareData($id, $step),
      '#lastStep' => $lastStep,
      '#finished' => $application->field_app_finished->value,
      '#employeeIsTesting' => $employeeIsTesting,
      '#ratings' => $ratings,
      '#active' => $application->field_app_active->value,
    ];

    return $returnArr;
  }

  public function getApplicationDetailTitle($id = NULL) {
    if ($id !== NULL) {
      $jobId = $this->queryHelper->getJobByApplicationId($id);
      $job = Job::load(reset($jobId));
      return $job->label();
    }
    return FALSE;
  }

  public function getArchiveTitle($id = NULL) {
    if ($id !== NULL) {
      $job = Job::load($id);
      return t('Weet u zeker dat u @job wil afronden?', ['@job' => $job->label()]);
    }
    return FALSE;
  }

  private function getFormOrRender($applicationId, $index, $currentStepIndex) {
    $index = $index + 1;
    $talk1 = $this->queryHelper->getStepsByIndexAndApplicationId($index, $applicationId);
    $form = NULL;
    if (isset($talk1)) {
//      if ($index !== $currentStepIndex) {
        $view_builder = $this->entityTypeManager->getViewBuilder('default_step');
        $pre_render = $view_builder->view($talk1, 'full');
        $form = render($pre_render);
//      } else {
        $form = $this->entityFormBuilder->getForm($talk1);
//      }
    }
    return $form;
  }

  private function getProfileValues($application)
  {
    // Get the current employee by the application.
    $employee = $application->getOwner();

    $return = [];
    $anon = $this->queryHelper->isJobAnon($application->id());
    if (!$anon) {
      // Name.
      $return['name'] = $employee->field_emp_firstname->value . ' ' . $employee->field_emp_name->value;

      // Age.
      $date = new \DateTime($employee->field_emp_birth->value);
      $now = new \DateTime('now');
      $interval = $now->diff($date);
      $return['age'] = $interval->y . ' ' . $this->t('jaar');

      // Phone.
      $return['phone'] = $employee->field_emp_phone->value;

      // Linkedin.
      $return['linkedin'] = $employee->field_emp_linkedin->value;

      // Gender.
      $return['sex'] = $employee->field_emp_sex->value;

      // Diploma
      $return['diploma'] = $employee->field_emp_diploma->value;

      // Picture.
      $image = NULL;
      if (isset($employee->get('field_emp_picture')->getValue()[0])) {
        $logoFid = $employee->get('field_emp_picture')->getValue()[0]['target_id'];
        if ($logoFid !== NULL) {
          $style = $this->entityTypeManager->getStorage('image_style')->load('picture');
          $image = $style->buildUrl(File::load($logoFid)->getFileUri());
        }
      }
      $return['image'] = $image;

      // Mail.
      $return['mail'] = $employee->getEmail();
    }

    return $return;
  }

  private function getStepFields($step, $applicationId)
  {
    $arr = [
      'Motivatie' => 'field_step1_motivation',
      'Werkervaring' => 'field_step1_experience_extra',
      'Extra diploma\'s en certificaten' => 'field_step1_diploma_extra',
    ];

    $fields = [];
    foreach ($arr as $key => $field) {
      if (isset($step->get($field)->getValue()[0]['value'])) {
        $label = $this->t($key)->__toString();
        $fields[$label] = $step->get($field)->getValue()[0]['value'];
      }
    }

    $jobId = $this->queryHelper->getJobByApplicationId($applicationId);
    $job = Job::load(reset($jobId));
    if (isset($job->field_job_extra_fields->getValue()[0])) {
      $extraFields = $job->field_job_extra_fields->getValue();
      $extraFieldsStep = $step->get('field_step1_extra_fields')->getValue()[0]['value'];
      $extraFieldsStep = json_decode($extraFieldsStep, TRUE);
      if (count($extraFields) > 0) {
        $extraFields = array_column($extraFields, 'value');
        foreach ($extraFields as $index => $extraField) {
          $fields[$extraField] = $extraFieldsStep[$index];
        }
      }
    }

    return $fields;
  }

  private function getStepExtraFields($step)
  {
    $fields = '<ul>';
    $fields .= str_replace(['<ul>', '</ul>'], ['', ''], $step->field_step1_languages->value);
    $arr = [
      'Niveau diploma' => 'field_step1_diploma',
      'Ervaringniveau' => 'field_step1_experience',
    ];
    foreach ($arr as $key => $field) {
      if (isset($step->get($field)->getValue()[0]['target_id'])) {
        $fields .= '<li>' . $this->t_ct($key) . ': ' . Term::load($step->get($field)->getValue()[0]['target_id'])->label() . '</li>';
      }
    }

    if (isset($step->get('field_step1_driver')->getValue()[0]['target_id'])) {
      $fields .= '<li>' . $this->t_ct('Rijbewijzen') . ': <ul>';
      foreach ($step->get('field_step1_driver')->getValue() as $fieldData) {
        $fields .= '<li>' . Term::load($fieldData['target_id'])->label() . '</li>';
      }
      $fields .= '</ul></li>';
    }

    $fields .= '</ul>';

    return $fields;
  }

  public function activateAccount($uuid = null)
  {
    $user = $this->entityRepository->loadEntityByUuid('user', $uuid);
    $user->activate();
    $user->save();


    if(in_array('employe', $user->getRoles())) {
      $response = new RedirectResponse(Url::fromRoute('user.login', ['user' => 'werknemer'])->toString());
    }

    if(in_array('company', $user->getRoles())) {
      $response = new RedirectResponse(Url::fromRoute('user.login', ['user' => 'werkgever'])->toString());
    }

    if($user->getLastLoginTime() == 0) {
      user_login_finalize($user);
      $response = new RedirectResponse(Url::fromRoute('general.user_controller_getCompanyProfilePage')->toString());
    };

    $response->send();


    // Redirect does not matter is in hook_user_login().
    //$response = new RedirectResponse(Url::fromRoute('user.login', ['user' => 'werkgever', 'mail' => $user->get('mail')->getValue()[0]['value']])->toString());
    //$response = new RedirectResponse(Url::fromRoute('view.vacatures.page_1')->toString());
  }

  private function getCompareData($applicationId, $step)
  {
    // Get the job by application
    $jobId = $this->queryHelper->getJobByApplicationId($applicationId);
    $job = Job::load(reset($jobId));

    // Check if waardenfit is set.
    if ($job->field_job_valuecompare->value === '1') {
      $empValues = $step->field_step1_values->referencedEntities();
      $compValues = $this->queryHelper->getValuesForCurrentUser($job->getOwnerId());
      $valueCompanyEntities = Value::loadMultiple($compValues);
      $valueComanyArr = [];
      foreach ($valueCompanyEntities as $valueCompanyEntity) {
        $valueComanyArr[$valueCompanyEntity->field_value_term->getValue()[0]['target_id']] = $valueCompanyEntity->field_value_rating->value;
      }

      $vals = [];
      foreach ($empValues as $empValue) {
        $term = $empValue->field_value_term->referencedEntities()[0];
        if ($term->hasTranslation($this->currentLanguage)) {
          $term = $this->entityRepository->getTranslationFromContext($term, $this->currentLanguage);
        }
        $vals[] = [
          'name' => $term->label(),
          'emp' => $empValue->field_value_rating->value,
          'comp' => $valueComanyArr[$term->id()],
        ];
      }
      return [
        'score' => $step->field_step1_value_score->value,
        'vals' => $vals,
      ];
    }

    return NULL;
  }
}


