<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Application entities.
 *
 * @ingroup general
 */
interface ApplicationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Application name.
   *
   * @return string
   *   Name of the Application.
   */
  public function getName();

  /**
   * Sets the Application name.
   *
   * @param string $name
   *   The Application name.
   *
   * @return \Drupal\general\Entity\ApplicationInterface
   *   The called Application entity.
   */
  public function setName($name);

  /**
   * Gets the Application creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Application.
   */
  public function getCreatedTime();

  /**
   * Sets the Application creation timestamp.
   *
   * @param int $timestamp
   *   The Application creation timestamp.
   *
   * @return \Drupal\general\Entity\ApplicationInterface
   *   The called Application entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Application published status indicator.
   *
   * Unpublished Application are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Application is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Application.
   *
   * @param bool $published
   *   TRUE to set this Application to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\general\Entity\ApplicationInterface
   *   The called Application entity.
   */
  public function setPublished($published);

}
