<?php

namespace Drupal\general\Form;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Form controller for Job edit forms.
 *
 * @ingroup general
 */
class JobForm extends ContentEntityForm {

  use ContextStringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t_ct('Maak nieuwe vacature aan');

    /* @var $entity \Drupal\general\Entity\Job */
    $form = parent::buildForm($form, $form_state);

    $form['status']['#attributes']['class'][] = 'hidden';

    $form['selectievragen'] = [
      '#markup' => '<div class="selection-questions"><h3>' . $this->t_ct('Selectievragen voor sollicitant') . '</h3><p>' . $this->t_ct('De standaardvragen die  de sollicitant moet invullen zijn motivatie, werkervaring en behaalde diploma’s/certificaten/attesten.') . '<br />' . $this->t_ct('Hieronder kan u nog enkele andere zaken aanduiden die u wilt bevragen. De sollicitant wordt gevraagd om aan te duiden in welke mate hij/zij dit beheerst.') . '<br />' .  $this->t_ct('Opgelet: u kan dit later niet meer aanpassen') . '</p></div>',
    ];

    $form['extra'] = [
      '#markup' => '<div class="extra info"><h3>' . $this->t_ct('Opgelet') . '</h3>' . '<p>' . $this->t_ct('Deze vacature wordt niet op externe kanalen gepubliceerd door ze hier aan te maken, je dient deze zelf op je eigen website, VDAB, of andere jobsites te verspreiden. Je moet hiervoor de deelbare link (die straks gegenereerd wordt) toevoegen in de vacature die je verspreidt. Vraag de sollicitanten om op deze link te klikken indien ze willen solliciteren. ') . '</p></div>',
    ];

    $form['field_job_languages']['widget']['#type'] = 'select2';
    $form['field_job_languages']['widget']['#multiple'] = TRUE;

    $form['open_vragen'] = [
      '#markup' => '<div class="open-questions"><h3>' . $this->t_ct('Open vragen') . '</h3><p>' . $this->t_ct('Hieronder kan u open vragen toevoegen die u wilt stellen aan de sollicitant(en). Vb. Hoe ken je ons bedrijf? Heb je een eigen wagen voor verplaatsingen?') . '<br />' . $this->t_ct('Klik op de knop “Voeg een extra open vraag toe” om nog vragen toe te voegen.') . '</p></div>',
    ];

    $form['extra_experience_driver'] = [
      '#markup' => '<div class="extra-driver-experience"><p>' . $this->t_ct('Wilt u bij de kandidaten bevragen hoeveel jaar werkervaring ze hebben en over welke rijbewijzen ze beschikken?') . '</p></div>  ',
    ];

    $form['field_job_extra_fields']['widget']['add_more']['#value'] = t('Voeg een extra open vraag toe');

    // Disable validation on edit form.
    if ($form_state->getBuildInfo()['form_id'] !== 'job_edit_form') {
      if (isset($form['field_job_valuecompare'])) {
        $form['#validate'][] = 'validate_values';
      }
      if (isset($form['field_job_sjt_categories'])) {
        $form['#validate'][] = 'validate_sjt';
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Job.', [
          '%label' => $entity->label(),
        ]));

        $entity->set('field_job_unique_id', user_password());
        $entity->save();
        $form_state->setRedirectUrl(Url::fromUserInput('/jobs'));

        break;

      default:
        drupal_set_message($this->t('Saved the %label Job.', [
          '%label' => $entity->label(),
        ]));
        $form_state->setRedirectUrl(Url::fromUserInput('/applications/job/' . $entity->id()));

    }
  }

}
