<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Url;
use Drupal\general\Entity\Application;
use Drupal\general\Form\JobAcceptForm;
use Drupal\general\Form\JobDeclineForm;
use Drupal\general\Form\JobStopForm;
use Drupal\general\Plugin\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class NextStepController.
 */
class NextStepController extends ControllerBase {

  /**
   * @var FormBuilder
   */
  protected $formBuilder;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * ApplicationController constructor.
   * @param FormBuilder $formBuilder
   * @param QueryHelper $queryHelper
   */
  public function __construct(FormBuilder $formBuilder, QueryHelper $queryHelper)
  {
    $this->formBuilder = $formBuilder;
    $this->queryHelper = $queryHelper;
  }

  /**
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('form_builder'),
      $container->get('general.query_helper')
    );
  }


  /**
   * Acceptemployee.
   * @param null $id
   */
  public function acceptEmployee($id = NULL) {
    // Get the current application.
    $application = Application::load($id);

    $currentStepIndex = (int)$application->field_app_current_step_index->value + 1;
    $steps = json_decode($application->field_app_all_steps->getValue()[0]['value'], TRUE);
    if ($currentStepIndex === count($steps)) {

      $application->set('field_app_current_step_index', $currentStepIndex);
      $application->set('field_app_finished', 1);
      $application->set('field_app_active', 0);
      $application->save();

      $jobId = $this->queryHelper->getJobByApplicationId($id);
      return new RedirectResponse('/applications/job/' . reset($jobId));
    }

    // Get the owner of the application.
    $name = $application->getOwner()->field_emp_firstname->value . ' ' . $application->getOwner()->field_emp_name->value;

    // Get the form.
    $form = $this->formBuilder->getForm(JobAcceptForm::class);

    return [
      '#theme' => 'application_accept',
      '#title' => $name,
      '#form' => $form,
    ];
  }

  /**
   * DeclineEmployee.
   * @param null $id
   * @return array
   */
  public function declineEmployee($id = NULL) {
    // Get the current application.
    $application = Application::load($id);

    // Get the owner of the application.
    $name = $application->getOwner()->field_emp_firstname->value . ' ' . $application->getOwner()->field_emp_name->value;

    // Get the form.
    $form = $this->formBuilder->getForm(JobDeclineForm::class);

    return [
      '#theme' => 'application_decline',
      '#title' => $name,
      '#form' => $form,
    ];
  }

  /**
   * StoppedByEmployee.
   * @param null $id
   * @return array
   */
  public function StoppedByEmployee($id = NULL) {
    // Get the form.
    $form = $this->formBuilder->getForm(JobStopForm::class);

    return [
      '#theme' => 'application_stop',
      '#title' => $this->t('Sollicitatie stopzetten?'),
      '#form' => $form,
    ];
  }



}
