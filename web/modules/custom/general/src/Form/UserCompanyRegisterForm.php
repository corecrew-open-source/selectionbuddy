<?php

namespace Drupal\general\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\general\Entity\Value;
use Drupal\general\Plugin\QueryHelper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Class UserCompanyRegisterForm.
 */
class UserCompanyRegisterForm extends FormBase
{

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Class constructor.
   * @param AccountInterface $account
   * @param LanguageManagerInterface $languageManager
   * @param QueryHelper $queryHelper
   * @param EntityRepositoryInterface $entityRepository
   * @param ConfigFactory $configFactory
   */
  public function __construct(AccountInterface $account, LanguageManagerInterface $languageManager, QueryHelper $queryHelper, EntityRepositoryInterface $entityRepository) {
    $this->account = $account;
    $this->languageManager = $languageManager;
    $this->queryHelper = $queryHelper;
    $this->entityRepository = $entityRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('general.query_helper'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'user_company_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($this->account->id() !== 0) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    $form['tip'] = [
      '#markup' => '<p class="form-description">' . $this->t('<b>Opgelet:</b> er kan slechts 1 emailadres worden gekoppeld aan uw bedrijfsprofiel.') . '</p>',
    ];

    $form['company'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bedrijf'),
    ];
    $form['mail_contactperson'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail contactpersoon'),
    ];
    $form['password'] = [
      '#type' => 'password_confirm',
      '#required' => TRUE,
    ];

    $alias = $this->queryHelper->getAliasByPath('/node/4');
    $form['privacy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ik ga akkoord met de <a target="_blank" href=":link">privacyverklaring</a>', [':link' => $alias]),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Bevestig'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
    $user = user_load_by_mail($form_state->getValue('mail_contactperson'));
    if ($user !== FALSE) {
      $form_state->setError($form['mail_contactperson'], t('Er bestaat al een gebruiker met dit e-mailadres'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();

    // Create user.
    $userValues = [
      'status' => 1,
      'name' => $values['mail_contactperson'],
      'mail' => $values['mail_contactperson'],
      'field_company_company' => $values['company']
    ];

    $user = User::create($userValues);
    $user->setPassword($values['password']);
    $user->addRole('company');
    $user->block();
    $user->save();

    // Create the values entities for the current user.
    // Get the taxonomies as template
    $languages =  $this->languageManager->getLanguages();
    $defaultLanguage =  $this->languageManager->getDefaultLanguage();
    $terms = Term::loadMultiple($this->queryHelper->getValuesTaxonomy());

    foreach($terms as $term) {
      // Create them for every language.
      $translatedTerm = $this->entityRepository->getTranslationFromContext($term, $defaultLanguage->getId());
      $value = Value::create([
        'status' => 1,
        'uid' => $user->id(),
        'name' => $translatedTerm->getName(),
        'field_value_term' => ['target_id' => $term->id()]
      ]);
      foreach ($languages as $language) {
        if($term->hasTranslation($language->getId()) && $language->getId() !== $defaultLanguage->getId()){
          $translatedTerm = $this->entityRepository->getTranslationFromContext($term, $language->getId());
          $value->addTranslation($language->getId(), [
            'name' => $translatedTerm->getName(),
          ]);
        }
      }
      $value->setOwnerId($user->id());
      $value->save();
    }

    // Send an email to the user.
    _user_mail_notify('register_no_approval_required', $user);

    // Redirect does not matter is in hook_user_login().
    $response = new RedirectResponse(Url::fromRoute('user.login', ['user' => 'werkgever', 'mail' => $values['mail_contactperson']])->toString());
    $response->send();
  }

}
