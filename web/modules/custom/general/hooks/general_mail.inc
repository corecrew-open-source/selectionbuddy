<?php

use \Drupal\Core\Render\Markup;

/**
 * Implements hook_mail().
 */
function general_mail_alter(&$message)
{
  $contextTranslationService = \Drupal::service('context_translation.translation');

  $mails = [
    'general_accept_employee_alert',
    'general_decline_employee_alert',
    'general_stop_employee_alert',
    'general_new_application',
    'general_tests_finished',
  ];
  if (in_array($message['id'], $mails)) {
    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8;';
    $message['html'] = true;
    $message['from'] = \Drupal::config('system.site')->get('mail');

    $message['subject'] = $message['params']['subject'];
    if ($message['id'] !== 'general_tests_finished') {
      $currentUserRoles = Drupal::currentUser()->getRoles();
      if (in_array('company', $currentUserRoles)) {
        $link = '/applications-employee';
      } else if (in_array('employe', $currentUserRoles)) {
        $link = '/jobs';
      }
    }

    $body = nl2br($message['params']['message']);
    if (isset($link)) {
      $body .= '<br/><br/>' . $contextTranslationService->t_ct('Klik') . ' <a href="' . $link . '">hier</a> ' . $contextTranslationService->t_ct('om je aan te melden bij Selectionbuddy en je profiel en sollicitatie online te beheren.');
    }
    $message['body'][] = Markup::create($body);

    if (isset($message['params']['reply-to'])) {
      $message['reply-to'] = $message['params']['reply-to'];
    }
  }

  $message['body'][] = $contextTranslationService->t_ct('Verzonden door Selectionbuddy.eu');
}
