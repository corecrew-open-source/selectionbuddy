<?php

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function paragraph_role_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the paragraph_role module.
    case 'help.page.paragraph_role':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('A module that defines what roles can view a paragraph added to a node') . '</p>';
      return $output;

    default:
  }
}
