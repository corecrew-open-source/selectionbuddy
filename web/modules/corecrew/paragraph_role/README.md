Add this to the composer.json in the root of you project

`"drupal/paragraphs_asymmetric_translation_widgets": {
    "Add paragraph_type to widget for paragraph_role module": "https://cgit.drupalcode.org/paragraphs_asymmetric_translation_widgets/patch/?id=c42f6b9c1ba041f148ccc6e3b91989a582794d67"
}`