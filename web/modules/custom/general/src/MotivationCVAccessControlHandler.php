<?php

namespace Drupal\general;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Motivation cv entity.
 *
 * @see \Drupal\general\Entity\MotivationCV.
 */
class MotivationCVAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\general\Entity\MotivationCVInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished motivation cv entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published motivation cv entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit motivation cv entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete motivation cv entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add motivation cv entities');
  }

}
