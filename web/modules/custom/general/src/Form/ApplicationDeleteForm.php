<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Application entities.
 *
 * @ingroup general
 */
class ApplicationDeleteForm extends ContentEntityDeleteForm {


}
