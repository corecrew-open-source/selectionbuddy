<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Sjtresult entities.
 *
 * @ingroup general
 */
class SJTresultDeleteForm extends ContentEntityDeleteForm {


}
