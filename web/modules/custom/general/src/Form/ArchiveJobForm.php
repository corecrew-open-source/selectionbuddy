<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\general\Entity\Application;
use Drupal\general\Entity\DefaultStep;
use Drupal\general\Entity\Job;
use Drupal\general\Entity\MotivationCV;
use Drupal\general\Entity\SJTresult;
use Drupal\general\Entity\Value;
use Drupal\general\Plugin\QueryHelper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class JobApplyForm.
 */
class ArchiveJobForm extends FormBase
{

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Class constructor.
   * @param AccountInterface $account
   * @param LanguageManagerInterface $languageManager
   * @param QueryHelper $queryHelper
   * @param EntityRepositoryInterface $entityRepository
   * @param RequestStack $requestStack
   */
  public function __construct(AccountInterface $account, LanguageManagerInterface $languageManager, QueryHelper $queryHelper, EntityRepositoryInterface $entityRepository, RequestStack $requestStack)
  {
    $this->account = $account;
    $this->languageManager = $languageManager;
    $this->queryHelper = $queryHelper;
    $this->entityRepository = $entityRepository;
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('general.query_helper'),
      $container->get('entity.repository'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'archive_job_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form_state->disableCache();
    if ($this->account->id() === 0 || !in_array('company', $this->account->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    $jobId = $this->request->get('id');
    if ($jobId !== NULL) {
      $form['job_id'] = [
        '#type' => 'hidden',
        '#value' => $jobId,
      ];
    }

    $form['extra'] = [
      '#markup' => '<p>' . $this->t('U dient de sollicitanten hiervan zelf op de hoogte te brengen. Let op! Eens gesloten kan deze vacature niet opnieuw geopend worden. ') . '</p>',
    ];

    $form['buttons']['annuleer'] = [
      '#type' => 'submit',
      '#value' => $this->t('Annuleer'),
      '#submit' => ['::cancelForm'],
      '#limit_validation_errors' => [],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Afronden'),
      '#submit' => array('::submitForm'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state)
  {
    $form_state->setRedirectUrl(Url::fromUserInput('/jobs'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    // Get the current application by the jobId and the userid.
    $jobEntity = Job::load($values['job_id']);
    $jobEntity->set('status', 0);
    $jobEntity->save();

    // Get all the applications for the job.
    $applications = $jobEntity->field_job_applications->referencedEntities();
    foreach ($applications as $app) {
      $app->set('field_app_active', 0);
      $app->save();
    }

    $form_state->setRedirectUrl(Url::fromUserInput('/jobs'));
  }
}
