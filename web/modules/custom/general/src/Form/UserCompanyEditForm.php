<?php

namespace Drupal\general\Form;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class UserCompanyEditForm.
 */
class UserCompanyEditForm extends FormBase {

  use ContextStringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_company_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $currentUser = \Drupal::currentUser();
    $user = User::load($currentUser->id());
    if ($currentUser->id() === 0 || !in_array('company', $currentUser->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    $form['formc_logo'] = [
      '#type' => 'managed_file',
      '#title' => t('Logo (optioneel)'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [2000000],
      ],
      '#description' => '<p><strong>' . $this->t_ct('Toegelaten bestandsextensies') . ':</strong> gif, png, jpg, jpeg</br><strong>' . $this->t_ct('Toegelaten bestandsgrootte') . ':</strong> 2MB</p>',
      '#theme' => 'image_widget',
      '#preview_image_style' => 'logo',
      '#upload_location' => 'public://logos',
      '#required' => FALSE,
    ];

    $form['formc_company'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Naam bedrijf'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['formc_slogan'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Slogan'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];

    $form['formc_sector'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Sector'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['formc_core_activity'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Kernactiviteiten'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['formc_street_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Straat + huisnummer'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['formc_postalcode_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Postcode + gemeente'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['formc_contactperson'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Contactpersoon'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['e_mail'] = [
      '#type' => 'email',
      '#title' => $this->t_ct('E-mail'),
      '#weight' => '0',
      '#default_value' => $user->getEmail(),
      '#required' => TRUE,
    ];
    $form['formc_website'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Website'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];


    // Set default values
    foreach ($form as $key => $value) {
      if (strpos($key, 'formc_') !== FALSE) {
        $field = $user->get(str_replace('formc_', 'field_company_', $key))->getValue();
        if (isset($field[0])) {
          if (isset($field[0]['value'])) {
            $form[$key]['#default_value'] = $field[0]['value'];
          }
          if (isset($field[0]['target_id'])) {
            $form[$key]['#default_value'] = [$field[0]['target_id']];
          }
        }
      }
    }

    $form['password'] = [
      '#type' => 'password_confirm',
      '#title' => '<strong>' . $this->t_ct('Wachtwoord aanpassen') . '</strong><p>' . $this->t_ct('Geef hieronder jouw nieuw wachtwoord in.') . '</p>',
      '#weight' => '0',
    ];


    $form['mails_fs'] = array(
      '#type' => 'fieldset',
      '#title' => '<strong>' . $this->t_ct('Voorkeuren') . '</strong><p>' . $this->t_ct('Indien u dit uitvinkt, krijgt u geen meldingen wanneer er nieuwe sollicitaties zijn en kan u de kandidaten enkel opvolgen door in te loggen op dit platform.') . '</p>',
    );

    $form['mails_fs']['mails'] = [
      '#type' => 'checkbox',
      '#title' => $this->t_ct('Ik wil graag mails ontvangen met updates over mijn vacatures'),
      '#weight' => '0',
      '#default_value' => 1,
    ];

    if (isset($user->get('field_mails')->getValue()[0])) {
      $form['mails']['#default_value'] = $user->get('field_mails')->getValue()[0]['value'];
    }

    $form['clearfix'] = [
    '#markup' => '<div class="clearfix"></div>'
    ];

    $form['annuleer'] = [
      '#type' => 'submit',
      '#value' => $this->t_ct('Annuleer'),
      '#submit' => ['::cancelForm'],
      '#limit_validation_errors' => [],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t_ct('Pas gegevens aan'),
      '#submit' => ['::submitForm'],
      '#attributes' => [
        'class' => [
          'button-primary'
        ]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('e_mail') !== $form['e_mail']['#default_value']) {
      $user = user_load_by_mail($form_state->getValue('e_mail'));
      if ($user !== FALSE) {
        $form_state->setError($form['e_mail'], $this->t_ct('Er bestaat al een gebruiker met dit e-mailadres'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $currentUser = \Drupal::currentUser();
    $user = User::load($currentUser->id());
    foreach ($values as $key => $value) {
      if (strpos($key, 'formc_') !== FALSE) {
        $user->set(str_replace('formc_', 'field_company_', $key), $value);
      }
    }

    $user->set('name', $values['e_mail']);
    $user->set('mail', $values['e_mail']);
    drupal_set_message($this->t_ct('Jouw profiel is succesvol opgeslagen'));
    if ($values['password'] !== '') {
      $user->setPassword($values['password']);
    }
    $user->set('field_mails', $values['mails']);
    $user->save();

    $form_state->setRedirect('general.user_controller_getCompanyProfilePage');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('general.user_controller_getCompanyProfilePage');
  }

}
