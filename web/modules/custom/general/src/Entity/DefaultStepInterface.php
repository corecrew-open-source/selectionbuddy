<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Default step entities.
 *
 * @ingroup general
 */
interface DefaultStepInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Default step name.
   *
   * @return string
   *   Name of the Default step.
   */
  public function getName();

  /**
   * Sets the Default step name.
   *
   * @param string $name
   *   The Default step name.
   *
   * @return \Drupal\general\Entity\DefaultStepInterface
   *   The called Default step entity.
   */
  public function setName($name);

  /**
   * Gets the Default step creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Default step.
   */
  public function getCreatedTime();

  /**
   * Sets the Default step creation timestamp.
   *
   * @param int $timestamp
   *   The Default step creation timestamp.
   *
   * @return \Drupal\general\Entity\DefaultStepInterface
   *   The called Default step entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Default step published status indicator.
   *
   * Unpublished Default step are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Default step is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Default step.
   *
   * @param bool $published
   *   TRUE to set this Default step to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\general\Entity\DefaultStepInterface
   *   The called Default step entity.
   */
  public function setPublished($published);

}
