/**
 * @file
 */

(function ($, Drupal) {

/**
 * MatchHeight settings
 */
// Under certain conditions where the size of the page is dynamically changing,
// such as during resize or when adding new elements,
// browser bugs cause the page scroll position to change unexpectedly.
$.fn.matchHeight._maintainScroll = true;
// By default, the _update method is throttled to execute at a maximum rate
// of once every 80ms.
$.fn.matchHeight._throttle = 80;
// Execute matchheight.
$.fn.matchHeight._update();

var matchHeightElements = [
  '.equal-heights article',
  '.view-sollicitaties-employee .views-row-inner',
];

/**
 * Media Queries
 */
var mobileQuery = 'screen and (max-width:699px)';
var nonMobileQuery = 'screen and (min-width:700px)';

/**
 * Example
 */
Drupal.behaviors.example = {
  attach: function (context, settings) {
    console.log('Drupal Behaviour');
  },
  mobile: function() {
    console.log('Mobile');
  },
  mobileUnmatch : function() {
    console.log('Unmatch Mobile');
  },
  nonMobile: function() {
    console.log('Non Mobile');
  }
}

$(document).ready(function(){

  //error message in sjt form
  $('.question-container').each(function(){
    if ($(this).find('.form-item--error-message').length == 1) {
      var errorMessage = $(this).find('.form-item--error-message');
      $(this).find('fieldset').each(function(){
        if ($(this).find('.form-item--error-message').length == 0) {
          errorMessage.clone().insertAfter($(this).find('legend'));
        }
      });
    }
  });

  $('.rating-form1, .rating-form2').addClass('rating-form');

  // adjust star rating
  showLowerValues();
  $('body').on('change', '.field--type-starrating input, .rating-form input, .personality-question-container input, #adjust-values-form input, #user-employee-form-step-3 input, .vacature_invullen #edit-container-values input', function(){
    showLowerValues();
  });
  $('body').on('mouseenter', '.field--type-starrating label, .rating-form label, .personality-question-container label, #adjust-values-form label, #user-employee-form-step-3 label, .vacature_invullen #edit-container-values label', function(){
    $(this).addClass('hover');
    hoverLowerValues();
  });
  $('body').on('mouseleave', '.field--type-starrating label, .rating-form label, .personality-question-container label, #adjust-values-form label, #user-employee-form-step-3 label, .vacature_invullen #edit-container-values label', function(){
    $(this).removeClass('hover');
    $('.field--type-starrating label, .rating-form label, .personality-question-container label, #adjust-values-form label, #user-employee-form-step-3 label, .vacature_invullen #edit-container-values label').removeClass('hovering');
    showLowerValues();
  });

  //shareable link info
  var tooltipText = Drupal.t('Kopieer deze link en plak deze in uw vacature <br>om sollicitanten via dit platform te laten solliciteren.');
  $('.view-vacatures #view-nothing-table-column').append('<a class="tooltip-handler">?</a><div class="tooltip">' + tooltipText + '</div>');
  $('.view-vacatures').on('click', '.tooltip-handler', function(e){
    e.preventDefault();
    e.stopPropagation();
    $(this).next().fadeIn();
  });
  $('body').on('click', function(){
    $('.view-vacatures .tooltip').fadeOut();
  });

  // sollicitaties popin
  $('.view-sollicitaties').on('click', '.popin-link', function(e){
    e.preventDefault();
    $('.pop-in').fadeIn();
  });
  $('.view-sollicitaties').on('click', '.popin-terug', function(e){
    e.preventDefault();
    $('.pop-in').fadeOut();
  });

  // solliciteren popin
  $('.anonymous_info').on('click', 'span', function(e){
    e.stopPropagation();
    var left = e.pageX - $('.anonymous_info ul').width() - 20;
    var top = e.pageY - $('.anonymous_info ul').height() - 50;
    $('.anonymous_info ul').css({'left': left, 'top': top}).fadeIn();
  });
  $('body').on('click', function(){
    $('.anonymous_info ul').fadeOut();
  });

// move testen into the same tab
  var testen2 = $('.tab.testen + .tab.testen').html()
  $('.tab.testen').append(testen2);
  $('.tab.testen + .tab.testen').remove();
  $('.tab.de-sollicatant-dient-de-test-nog-in-te-vullen + .tab.de-sollicatant-dient-de-test-nog-in-te-vullen').remove();
// vacature info
var back = Drupal.t('back');
var more = Drupal.t('Meer info');
$('.vacature_invullen .sidebar').on('click', '.button:not(.back)', function(e){
  e.preventDefault();
  $(this).addClass('back').text(back);
  $('.vacature_invullen .sidebar .qm').css('display', 'block');
  $('.vacature_invullen .content .job-content').css('display', 'block');
  $('.vacature_invullen .content .tab-content').css('display', 'none');
});
$('.vacature_invullen .sidebar').on('click', '.back', function(e){
  e.preventDefault();
  $(this).removeClass('back').text(more);
  $('.vacature_invullen .sidebar .qm').css('display', 'none');
  $('.vacature_invullen .content .tab-content').css('display', 'block');
  $('.vacature_invullen .content .job-content').css('display', 'none');
  $('.vacature_invullen .content .company-content').css('display', 'none');
});
$('.vacature_invullen .sidebar').on('click', '.qm', function(e){
  e.preventDefault();
  $('.vacature_invullen .sidebar .qm').css('display', 'none');
  $('.vacature_invullen .content .job-content').css('display', 'none');
  $('.vacature_invullen .content .company-content').css('display', 'block');
});

// vacature aanmaken
$('.job-add-form .selection-questions').insertBefore('.job-add-form .selectievragen');
$('.job-add-form .extra-driver-experience').insertBefore('.job-add-form .field--name-field-job-experience');
$('.job-add-form .open-questions').insertBefore('.job-add-form .field--name-field-job-extra-fields');
$('.job-add-form #edit-actions').once().css('display', 'none');
var next = Drupal.t('Next');
var stop = Drupal.t('Stop');
var save = Drupal.t('Save');
var prev = Drupal.t('Previous');
$('<div class="tab-buttons"><button class="stop">'+stop+'</button> <button class="button-primary nextTab">'+next+'</button></div>').insertAfter('.job-add-form #edit-actions');
  if ($('.job-add-form .horizontal-tab-button-1').hasClass('selected')) {
    $('.job-add-form .tab-buttons .nextTab').text(save);
    $('.job-add-form .tab-buttons .nextTab').removeClass('nextTab').addClass('save');
    $('.job-add-form .tab-buttons .stop').text(prev);
    $('.job-add-form .tab-buttons .stop').removeClass('stop').addClass('prev');
  }
$('.job-add-form .tab-buttons').on('click', '.nextTab', function(e){
  e.preventDefault();
  $(this).text(save);
  $(this).removeClass('nextTab').addClass('save');
  $('.job-add-form .tab-buttons .stop').text(prev);
  $('.job-add-form .tab-buttons .stop').removeClass('stop').addClass('prev');
  $('.job-add-form .horizontal-tab-button-1 a').click();
});
$('.job-add-form .tab-buttons').on('click', '.save', function(e){
  e.preventDefault();
  $('.job-add-form #edit-actions #edit-submit').click();
});
// $('.job-add-form .tab-buttons').on('click', '.stop', function(e){
//   e.preventDefault();
//   $('.job-add-form #edit-actions .cancel-button').click();
// });
$('.job-add-form .tab-buttons').on('click', '.prev', function(e){
  e.preventDefault();
  $(this).text(stop);
  $(this).removeClass('prev').addClass('stop');
  $('.job-add-form .tab-buttons .save').text(next);
  $('.job-add-form .tab-buttons .save').removeClass('save').addClass('nextTab');
  $('.job-add-form .horizontal-tab-button-0 a').click();
});
$('.job-add-form').on('click', '.horizontal-tab-button-0 a', function(e){
    $('.job-add-form .tab-buttons .prev').text(stop);
    $('.job-add-form .tab-buttons .prev').removeClass('prev').addClass('stop');
    $('.job-add-form .tab-buttons .save').text(next);
    $('.job-add-form .tab-buttons .save').removeClass('save').addClass('nextTab');
});
$('.job-add-form').on('click', '.horizontal-tab-button-1 a', function(e){
    $('.job-add-form .tab-buttons .stop').text(prev);
    $('.job-add-form .tab-buttons .stop').removeClass('stop').addClass('prev');
    $('.job-add-form .tab-buttons .nextTab').text(save);
    $('.job-add-form .tab-buttons .nextTab').removeClass('nextTab').addClass('save');
});

// login
if ($('.user-login-form').length > 0) {
  var form = getUrlParameter('user');
  if (form == 'sollicitant') {$('.user-login-form').addClass('werknemer');}
  if (form == 'werkgever') {
    $('.user-login-form').addClass('werkgever');
    $('.user-login-form .button.register').attr('href', '/general/form/user_company');
  }
  var destination = getUrlParameter('destination');
  if (destination != undefined) {
    if (sessionStorage.companyName != undefined) {
      $('.user-login-form').addClass('vacature');
      $('.user-login-form .vacature .job-title').text(sessionStorage.jobTitle);
      $('.user-login-form .vacature .company-name').text(sessionStorage.companyName);
    }
    if (sessionStorage.companyLogo != 'undefined') {
      $('.user-login-form .vacature .company-logo').append('<img src="'+sessionStorage.companyLogo+'">');
    }
  }
}
$('.application_anonymous').on('click', '.button', function(e){
  sessionStorage.companyLogo = $('.application_anonymous .job .logo img').attr('src');
  sessionStorage.jobTitle = $('.application_anonymous .job .job-title').text().trim();
  sessionStorage.companyName = $('.application_anonymous .job .company').text().trim();
});



// vacatures
if ($('.view-vacatures').length > 0) {
  $('#block-pagetitle').addClass('vacature-view-title');
  $('.view-vacatures .view-filters form').addClass('vacature-filters').appendTo('#block-pagetitle');
  $('.view-vacatures .view-header a').addClass('vacature-button').appendTo('#block-pagetitle');
}

// select form items
$('select:not(.select2-hidden-accessible)').wrap('<div class="select"></div>');
$('.select2-container').wrap('<div class="select"></div>');


// tab navigation for employer
$('.application-company .tabs-wrapper .tabs').on('click', 'li:not(.disabled)', function(){
  var index = $(this).index() + 1;

  $('.application-company .tabs-wrapper .current').removeClass('current');
  $('.application-company .tabs-wrapper .tabs li:nth-child('+index+')').addClass('current');
  $('.application-company .tabs-wrapper .tab:nth-child('+index+')').addClass('current');

  $('.application-company .tabs-wrapper .tab:nth-child('+index+') input[name="index_tab"]').val(index);
});


  var tab = getUrlParameter('tab');
  if (tab != undefined) {
    $('.application-company .content .tabs li:nth-child('+tab+')').click();
  }

//tab testen for employer
$('.tab.testen').on('click', '.cat_score', function(){
  if (!$(this).hasClass('active')) {
    $(this).addClass('active');
    $(this).next().slideDown();
  } else {
    $(this).removeClass('active');
    $(this).next().slideUp();
  }
});


}); // document.ready


  function showLowerValues(){
    $('.field--type-starrating .form-radios, .rating-form .form-radios, .personality-question-container .form-radios, #adjust-values-form .form-radios, #user-employee-form-step-3 .form-radios, .vacature_invullen #edit-container-values .form-radios').each(function(){
      $(this).find('label').removeClass('lowerValue');
      if ($(this).find('input:checked').length > 0) {
        $(this).find('input').each(function(){
          if ($(this).is(':checked')) {
            return false;
          } else {
            $(this).next().addClass('lowerValue');
          }
        });
      }
    });
  }

  function hoverLowerValues(hover){
    $('.field--type-starrating .hover, .rating-form .hover, .personality-question-container .hover, #adjust-values-form .hover, #user-employee-form-step-3 .hover, .vacature_invullen #edit-container-values .hover').parent().parent().each(function(){
      $(this).find('label').removeClass('lowerValue');
      $(this).find('label').addClass('hovering');
      $(this).find('input').each(function(){
        if ($(this).next().hasClass('hover')) {
          return false;
        } else {
          $(this).next().addClass('lowerValue');
        }
      });
    });
  }


function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function copyToClipboard(elem, button) {
  // create hidden text element to copy content from
  var target = document.createElement("textarea");
  target.style.position = "absolute";
  target.style.left = "-9999px";
  target.style.top = "0";
  document.body.appendChild(target);
  target.textContent = elem;
  // select the content
  var currentFocus = document.activeElement;
  target.focus();
  target.setSelectionRange(0, target.value.length);
  // copy the selection
  var succeed;
  try {
      succeed = document.execCommand("copy");
  } catch(e) {
      succeed = false;
  }
  // restore original focus
  if (currentFocus && typeof currentFocus.focus === "function") {
      currentFocus.focus();
  }
  // clear temporary content
  target.remove();
  // show message
  if (succeed) {
    button.next().fadeIn();
    setTimeout(function() {
      button.next().fadeOut();
    }, 1000);
  }
  return succeed;
}


// equalHeights on images loaded
Drupal.behaviors.imagesLoaded = {
  attach: function (context, settings) {
    var loaded = 0;
    var noOfImages = $("img").length;
    $("img").on('load', function(){
      loaded++;
      if(noOfImages === loaded) {
        $(matchHeightElements.join(', ')).matchHeight({ byRow: true, property: 'min-height' });
      }
    }).each(function(){
      if (this.complete) {
        $(this).trigger('load');
      }
    });
  },
};

// table sort
Drupal.behaviors.tableSort = {
  attach: function (context, settings) {
    $('.tablesort').each(function(){
      var sort = $(this).attr('class').split(' tablesort--');
      sort = sort[1];
      $(this).parent().parent().addClass(sort);
    });
  }
};

// shareable url
Drupal.behaviors.shareableURL = {
  attach: function (context, settings) {
    $('.view-vacatures td.views-field-nothing').each(function(){
      var url = $(this).text().trim();
      $(this).wrapInner('<span class="url"></span>');
      $(this).append('<a href="'+url+'"></a>');
      $(this).append('<span class="copied">Link gekopie&euml;rd</span>');
    });
    $('.view-vacatures').on('click', '.views-field-nothing a:not(.tooltip-handler)', function(e){
      e.preventDefault();
      var href = $(this).attr('href');
      var button = $(this);
      copyToClipboard(href, button);
    });
  }
};
/**
 * Toggle local tasks
 * show/hide local tasks
 */

Drupal.behaviors.corecrewActionLinksToggle = {
  attach: function (context, settings) {
    $(context).find('.region--header').once('corecrewActionLinksToggle').each(function () {
      var actionLinks = $('.block-local-tasks-block');
      var cog = $('.action-links-toggle');
      actionLinks.addClass('hidden');
      cog.on('click', function() {
          actionLinks.toggleClass('hidden');
      });
    });
  }
};

/**
 * Toggle mobile header
 * show/hide mobile header
 */
Drupal.behaviors.toggleMobileHeader = {
  attach: function (context, settings) {
    $('.region--header').once().on('click', '#mobile-menu-anchor', function(e){
      e.preventDefault();
      if ($(this).hasClass('is-active')) {
        $('.region--header-mobile-inner').stop().slideUp();
      } else {
        $('.region--header-mobile-inner').stop().slideDown();
      }
      $(this).toggleClass('is-active');
    });
  },
};

/**
 * Accordeon
 * paragraph accordeon
 */
Drupal.behaviors.accordeon = {
  attach: function (context, settings) {
    $('.paragraph--type--accordeon').once().on('click', '.field--name-field-title', function(e){
      e.preventDefault();
      if ($(this).hasClass('active')) {
        $(this).removeClass('active').next().slideUp();
      } else {
        $(this).parent().parent().parent().find('.field--name-field-title.active').removeClass('active').next().slideUp();
        $(this).addClass('active').next().slideDown();
      }
    });
  },
};


/**
 * Initialize Enquire
 */
enquire.register(mobileQuery, {

  match : function() {
    Drupal.behaviors.example.mobile();
  },

  unmatch : function() {
    Drupal.behaviors.example.mobileUnmatch();
  },

  setup : function() {},
  deferSetup : true,

  destroy : function() {}

}).register(nonMobileQuery, {
  match : function() {
    Drupal.behaviors.example.nonMobile();
    $(matchHeightElements.join(', ')).matchHeight({ byRow: true, property: 'min-height' });
  },

  unmatch : function() {
    $(matchHeightElements.join(', ')).matchHeight({ property: 'min-height', remove: true });
  },

  setup : function() {},
  deferSetup : true,

  destroy : function() {}

});

$(document).ready(function(){

  $('input[type="submit"].no-submit').on('click', function (e) {
    e.preventDefault();
  });

  CKEDITOR.on('instanceReady', function () {
    // Get the HTML ID of the textarea input that would be used if the editor were not enabled:
    var fieldID = 'edit-field-step-text-0-value';
    if (CKEDITOR.instances[fieldID]) {
      // Add keyup listener.
      CKEDITOR.instances[fieldID].on("key", function () {
        // The last key pressed isn't available in editor.getData() when
        // the key is pressed. A workaround is to use setTimeout(), with no
        // time set to it, as this moves it to the end of the process queue,
        // when the last pressed key will be available.
        var editor = this;
        window.setTimeout(function () {
          var text = editor.getData();
          // Do something with the text.
          // Also note that the original textarea can be
          // retrieved from editor.element.$. So you could
          // do this:
          var originalTextarea = editor.element.$;
          $('.application-company .header').addClass('inactive');
          $('.form-actions input').addClass('button-primary');
        });
      });
    }
  });

  $('.application-company').on('click', '.header.inactive a', function(e){
    e.preventDefault();
  });

  /*$('.rating-form input').on('change', function(){
        $('.application-company .header').addClass('inactive');
        $('.form-actions input').addClass('button-primary');
        $('.rating-form1 .form-submit').addClass('button-primary');
        $('.rating-form2 .form-submit').addClass('button-primary');
  });
  $('.field--type-starrating input').on('change', function(){
        $('.application-company .header').addClass('inactive');
        $('.form-actions input').addClass('button-primary');
  });*/

});

})(jQuery, Drupal, CKEDITOR);
