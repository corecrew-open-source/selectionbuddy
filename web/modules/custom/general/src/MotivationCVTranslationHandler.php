<?php

namespace Drupal\general;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for motivation_c_v.
 */
class MotivationCVTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
