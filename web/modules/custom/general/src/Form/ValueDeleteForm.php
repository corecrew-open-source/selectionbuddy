<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Value entities.
 *
 * @ingroup general
 */
class ValueDeleteForm extends ContentEntityDeleteForm {


}
