<?php

use \Drupal\Core\Entity\EntityInterface;
use \Drupal\Core\Session\AccountInterface;
use \Drupal\Core\Access\AccessResult;

/**
 * Implements HOOK_entity_access().
 */
function paragraph_role_paragraph_access(EntityInterface $entity, $operation, AccountInterface $account)
{
  // Get the roles for the current user.
  $currentUserRoles = $account->getRoles();

  // Check if operation is view & bypass for god and custom permission.
  if ($operation === 'view' && $account->id() !== 1 && !$account->hasPermission('bypass paragraph role check')) {
    // Get the entity type and fieldname.
    $type = $entity->getType();
    $fieldname = 'field_' . $type . '_roles';

    // Check if paragraph has the field enabled.
    if ($entity->hasField($fieldname)) {
      // Get the roles from the paragraph.
      $availableRoles = $entity->get($fieldname)->getValue();
      $availableRoles = array_column($availableRoles, 'target_id');
      // If there are no roles enabled everyone can view.
      if (count($availableRoles) === 0) {
        return AccessResult::allowed();
      } else {
        // Check if the user has one of the roles selected.
        $intersect = array_intersect($availableRoles, $currentUserRoles);
        if (!empty($intersect)) {
          return AccessResult::allowed();
        } else {
          return AccessResult::forbidden();
        }
      }
    }
  }
}
