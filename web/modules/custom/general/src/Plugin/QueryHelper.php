<?php

namespace Drupal\general\Plugin;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\AliasManager;
use Drupal\Core\Session\AccountProxy;
use Drupal\general\Entity\Application;
use Drupal\general\Entity\DefaultStep;
use Drupal\general\Entity\Job;
use Drupal\general\Entity\MotivationCV;
use Drupal\taxonomy\Entity\Term;

/**
 * Class QueryHelper.
 */
class QueryHelper
{

  use ContextStringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  protected $currentUser;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var string
   */
  protected $currentLanguage;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var ConfigFactory
   */
  protected $configFactory;

  /**
   * @var AliasManager
   */
  protected $aliasManager;

  /**
   * Constructor for the
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param AccountProxy $currentUser
   * @param LanguageManagerInterface $languageManager
   * @param EntityRepositoryInterface $entityRepository
   * @param ConfigFactory $configFactory
   * @param AliasManager $aliasManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxy $currentUser, LanguageManagerInterface $languageManager, EntityRepositoryInterface $entityRepository, ConfigFactory $configFactory, AliasManager $aliasManager)
  {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $entityTypeManager->getStorage('user')->load($currentUser->id());
    $this->languageManager = $languageManager;
    $this->currentLanguage = $languageManager->getCurrentLanguage()->getId();
    $this->entityRepository = $entityRepository;
    $this->configFactory = $configFactory;
    $this->aliasManager = $aliasManager;
  }

  public function getValuesForCurrentUser($userid = NULL)
  {
    if ($userid === NULL) {
      $userid = $this->currentUser->id();
    }
    $query = $this->entityTypeManager->getStorage('value')->getQuery();
    $query->condition('user_id', $userid);
    $query->condition('field_value_jobvalue', 0);
    return $query->execute();
  }

  public function getValuesTaxonomy()
  {
    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $query->condition('vid', 'waarden');
    $query->sort('weight');
    $tids = $query->execute();
    return $tids;
  }

  public function getTaxonomyFormArr($vid, $dataType = 'label')
  {
    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $query->condition('vid', $vid);
    $query->sort('weight');
    $tids = $query->execute();

    $dataArr = [];
    $terms = Term::loadMultiple($tids);
    foreach ($terms as $tid => $term) {
      if ($term->hasTranslation($this->currentLanguage)) {
        $translatedTerm = $this->entityRepository->getTranslationFromContext($term, $this->currentLanguage);
        if ($dataType === 'label') {
          $dataArr[$tid] = $translatedTerm->label();
        } else {
          $dataArr[$tid] = $translatedTerm;
        }
      }
    }
    return $dataArr;
  }

  public function getTaxonomyFormArrByArr($terms)
  {
    $dataArr = [];
    foreach ($terms as $tid => $term) {
      if ($term->hasTranslation($this->currentLanguage)) {
        $translatedTerm = $this->entityRepository->getTranslationFromContext($term, $this->currentLanguage);
        $dataArr[$term->id()] = $translatedTerm->label();
      }
    }
    return $dataArr;
  }

  public function getJobByUniqueId($id)
  {
    $query = $this->entityTypeManager->getStorage('job')->getQuery();
    $query->condition('field_job_unique_id', $id);
    return $query->execute();
  }

  public function getJobByApplicationId($id)
  {
    $query = $this->entityTypeManager->getStorage('job')->getQuery();
    $query->condition('field_job_applications', $id, 'IN');
    return $query->execute();
  }

  public function getApplicationByStep($step)
  {
    return $step->field_step_application->referencedEntities()[0];
  }

  public function getTabsforCurrentJobEmployee($job)
  {
    $steps = [
      'Start sollicitatie'
    ];
    if ($job->field_job_acquaintance->value === '1') {
      $steps[] = 'Kennismaking';
    }
    if ($job->field_job_personalities->value === '1' || $job->field_job_sjt->value === '1') {
      $steps[] = 'Testen';
    }
    if ($job->field_job_talk_1->value === '1') {
      $steps[] = 'Gesprek';
    }
    if ($job->field_job_talk_2->value === '1') {
      $steps[] = 'Gesprek 2';
    }

    return $steps;
  }

  public function getTabsforCurrentJobCompany($application)
  {
    $jobId = $this->getJobByApplicationId($application->id());
    $job = Job::load(reset($jobId));
    $steps = [
      'Motivatie & CV'
    ];
    if ($job->field_job_valuecompare->value === '1') {
      $steps[] = 'Waardenfit';
    }
    if ($job->field_job_acquaintance->value === '1') {
      $steps[] = 'Kennismaking';
    }
    if ($job->field_job_personalities->value === '1' || $job->field_job_sjt->value === '1') {
      $steps[] = 'Testen';
    }
    if ($job->field_job_talk_1->value === '1') {
      $steps[] = 'Gesprek';
    }
    if ($job->field_job_talk_2->value === '1') {
      $steps[] = 'Gesprek 2';
    }
    return $steps;
  }

  public function getStepsByIndexAndApplicationId($index, $applicationId)
  {
    $application = Application::load($applicationId);
    $data = json_decode($application->field_app_all_steps->getValue()[0]['value']);
    $stepRaw = $data[$index - 1];
    if ($stepRaw->type === 'motivation_c_v') {
      return MotivationCV::load($stepRaw->id);
    } else if ($stepRaw->type === 'default_step') {
      return DefaultStep::load($stepRaw->id);
    } else if ($stepRaw->type === 'tests') {
      return 'tests';
    }

    return FALSE;
  }

  public function getTestStepForUserAndApplication($applicationId, $userid = NULL)
  {
    if ($userid === NULL) {
      $userid = $this->currentUser->id();
    }
    $query = $this->entityTypeManager->getStorage('sjt_result')->getQuery();
    $query->condition('user_id', $userid);
    $query->condition('field_sjt_application', $applicationId);
    $data = $query->execute();

    $query = $this->entityTypeManager->getStorage('perso_result')->getQuery();
    $query->condition('user_id', $userid);
    $query->condition('field_perso_application', $applicationId);
    $data = array_merge($data, $query->execute());

    return $data;
  }

  public function isJobAnon($id, $type = 'application')
  {
    $jobId = $id;
    if ($type === 'application') {
      $jobId = $this->getJobByApplicationId($id);
      $jobId = reset($jobId);
    }
    $job = Job::load($jobId);

    if (isset($job->get('field_job_anon')->getValue()[0]) && $job->get('field_job_anon')->getValue()[0]['value'] === '1') {
      if ($type === 'job') {
        return TRUE;
      }
      $application = Application::load($id);
      $currentStepIndex = 0;
      if ($application) {
        $currentStepIndex = (int)$application->field_app_current_step_index->value + 1;
      }
      if ($currentStepIndex > 1) {

        return FALSE;
      }
      return TRUE;
    }
    return FALSE;
  }

  public function getCurrentStepRatingIndex($application, $type = FALSE, $next = FALSE)
  {
    $currentStepIndex = (int)$application->field_app_current_step_index->value;
    if ($next) {
      $currentStepIndex++;
    }
    $steps = json_decode($application->field_app_all_steps->getValue()[0]['value'], TRUE);
    $indexRating = 0;
    if (isset($steps[$currentStepIndex]['type'])) {
      switch ($steps[$currentStepIndex]['type']) {
        case 'motivation_c_v':
          $indexRating = 1;
          break;
        case 'default_step':
          switch ($steps[$currentStepIndex]['subtype']) {
            case 'acc':
              $indexRating = 2;
              break;
            case 'talk1':
              $indexRating = 4;
              break;
            case 'talk2':
              $indexRating = 5;
              break;
          }
          break;
        case 'tests':
          $indexRating = 3;
          break;
      }
    }
    if ($type) {
      if ($steps[$currentStepIndex]['type'] === 'motivation_c_v' || $steps[$currentStepIndex]['type'] === 'tests') {
        return $steps[$currentStepIndex]['type'];
      }
      else if ($steps[$currentStepIndex]['type'] === 'default_step') {
        return $steps[$currentStepIndex]['subtype'];
      }

    }
    return $indexRating;
  }

  public function getAliasByPath($path)
  {
    $alias = $this->aliasManager->getAliasByPath($path);
    $langObject = $this->languageManager->getCurrentLanguage();
    $language = $langObject->getId();

    $languagePrefix = $this->configFactory->get('language.negotiation')->get('url')['prefixes'][$language];
    if ($languagePrefix !== '') {
      $alias = '/' . $languagePrefix . $alias;
    }

    return $alias;
  }

  public function getDefaultValuePerstep($applicationId, $status = 'accept')
  {
    $application = Application::load($applicationId);
    $stepType = $this->getCurrentStepRatingIndex($application, TRUE, FALSE);
    if ($status === 'accept') {
      $stepType = $this->getCurrentStepRatingIndex($application, TRUE, TRUE);
    }
    if (strpos($stepType, 'talk') !== FALSE) {
      $stepType = 'talk';
    }
    $text = $this->t_ct('STEP_' . strtoupper($status) . '_' . strtoupper($stepType) . '_TEXT');
    if ($status === 'stop') {
      $text = $this->t_ct('STEP_' . strtoupper($status) . '_TEXT');
    }

    // Replace tokens in text.
    $text = $text->__toString();
    $job = $this->getJobByApplicationId($applicationId);
    $job = Job::load(reset($job));
    $companyUser = $job->getOwner();
    $applicationUser = $application->getOwner();
    $tokens = [];
    if (strpos($text, '[functietitel]') !== FALSE) {
      $tokens['[functietitel]'] = $job->label();
    }
    if (strpos($text, '[naam bedrijf]') !== FALSE || strpos($text, '[bedrijf]') !== FALSE) {
      $tokens['[bedrijf]'] = $companyUser->get('field_company_company')->value;
      $tokens['[naam bedrijf]'] = $companyUser->get('field_company_company')->value;
    }
    if (strpos($text, ' [naam kandidaat]') !== FALSE) {
      $tokens[' [naam kandidaat]'] = ' ' . $applicationUser->get('field_emp_firstname')->value . ' ' . $applicationUser->get('field_emp_name')->value;
      if ($this->isJobAnon($applicationId)) {
        $tokens[' [naam kandidaat]'] = '';
      }
    }
    if (strpos($text, ' [voornaam kandidaat]') !== FALSE) {
      $tokens[' [voornaam kandidaat]'] = ' ' . $applicationUser->get('field_emp_firstname')->value;
      if ($this->isJobAnon($applicationId)) {
        $tokens[' [voornaam kandidaat]'] = '';
      }
    }
    if (strpos($text, '[naam contactpersoon]') !== FALSE) {
      $tokens['[naam contactpersoon]'] = $companyUser->get('field_company_contactperson')->value;
    }
    if (strpos($text, '[emailadres contactpersoon]') !== FALSE) {
      $tokens['[emailadres contactpersoon]'] = $companyUser->getEmail();
    }

    return str_replace(array_keys($tokens), array_values($tokens), $text);
  }

  public function getDataToDeleteUser($type)
  {
    // Type can be 'application', 'perso_result', 'sjt_result' or 'value'
    $userid = $this->currentUser->id();
    $query = $this->entityTypeManager->getStorage($type)->getQuery();
    $query->condition('user_id', $userid);
    $data = $query->execute();
    return $data;
  }
}
