<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Motivation cv edit forms.
 *
 * @ingroup general
 */
class MotivationCVForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\general\Entity\MotivationCV */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Motivation cv.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Motivation cv.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.motivation_c_v.canonical', ['motivation_c_v' => $entity->id()]);
  }

}
