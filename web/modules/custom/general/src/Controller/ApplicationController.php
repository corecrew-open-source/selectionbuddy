<?php

namespace Drupal\general\Controller;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\general\Entity\Job;
use Drupal\general\Form\JobApplyForm;
use Drupal\general\Form\SjtCreateForm;
use Drupal\general\Plugin\QueryHelper;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ApplicationController.
 */
class ApplicationController extends ControllerBase
{

  use ContextStringTranslationTrait;

  /**
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var FormBuilder
   */
  protected $formBuilder;

  /**
   * ApplicationController constructor.
   * @param AccountProxyInterface $currentUser
   * @param QueryHelper $queryHelper
   * @param EntityTypeManager $entityTypeManager
   * @param FormBuilder $formBuilder
   */
  public function __construct(AccountProxyInterface $currentUser, QueryHelper $queryHelper, EntityTypeManager $entityTypeManager, FormBuilder $formBuilder)
  {
    $this->queryHelper = $queryHelper;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
  }

  /**
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    $currentUser = $container->get('current_user');
    $queryHelper = $container->get('general.query_helper');
    $entityTypeManager = $container->get('entity_type.manager');
    $formbuilder = $container->get('form_builder');
    return new static($currentUser, $queryHelper, $entityTypeManager, $formbuilder);
  }

  /**
   * Jobapplication.
   */
  public function jobApplication($id = NULL)
  {
    if ($id === NULL) {
      return new RedirectResponse(Url::fromRoute('user.login')->toString());
    }

    // Load the job.
    $jobId = $this->queryHelper->getJobByUniqueId($id);
    $jobId = reset($jobId);
    $job = Job::load($jobId);

    // Get the company of the job.
    $userCompany = $job->user_id->referencedEntities()[0];

    // Get the logo
    $image = NULL;
    if (isset($userCompany->get('field_company_logo')->getValue()[0])) {
      $logoFid = $userCompany->get('field_company_logo')->getValue()[0]['target_id'];
      if ($logoFid !== NULL) {
        $style = $this->entityTypeManager->getStorage('image_style')->load('logo');
        $image = $style->buildUrl(File::load($logoFid)->getFileUri());
      }
    }

    // Check if the current logged in employee has already a application for the job. If not show the apply page.
    $jobApplications = $job->field_job_applications->referencedEntities();
    $userArr = [];
    foreach ($jobApplications as $application) {
      $applicationUser = $application->getOwnerId();
      $userArr[] = $applicationUser;
    }
    if (in_array($this->currentUser->id(), $userArr) === FALSE) {
      if ($job->isPublished()) {
        setcookie('job_id', $id, time() + 31536000, '/');
      }

      return [
        '#theme' => 'application_anonymous',
        '#title' => '',
        '#company' => $userCompany->field_company_company->value,
        '#jobtitle' => $job->label(),
        '#logo' => $image,
        '#jobid' => $id,
        '#active' => $job->isPublished(),
      ];
    } else {
      // Redirect to current step
      return new RedirectResponse(Url::fromRoute('general.application_controller_jobApplicationApply', ['id' => $id])->toString());
    }
  }


  public function jobApplicationPageTitle($id = NULL)
  {
    if ($id === NULL) {
      return new RedirectResponse(Url::fromRoute('user.login')->toString());
    }

    // Load the job.
    $jobId = $this->queryHelper->getJobByUniqueId($id);
    $jobId = reset($jobId);
    $job = Job::load($jobId);

    return $job->label();
  }



  /**
   * Jobapplication.
   *
   * @return string
   *   Return Hello string.
   */
  public function jobApplicationApply($id = NULL)
  {
    // Check if the current user is an employee
    if (in_array('employe', $this->currentUser->getRoles()) !== FALSE) {
      // Load the job.
      $jobId = $this->queryHelper->getJobByUniqueId($id);
      $jobId = reset($jobId);
      if ($jobId === FALSE) {
        return new RedirectResponse(Url::fromRoute('<front>')->toString());
      }
      $job = Job::load($jobId);


      // Remove the cookie.
      if (isset($_COOKIE['job_id']) && $_COOKIE['job_id'] !== NULL && $_COOKIE['job_id'] == $id) {
        $jobId = $_COOKIE['job_id'];
        // Unset the cookie
        unset($_COOKIE['job_id']);
        setcookie('job_id', '', 1, '/');
      }

      if ($job->isPublished()) {
        // Get the form
        $form = $this->formBuilder->getForm(JobApplyForm::class);
        $currentApplicationId = NULL;
        $finished = NULL;

        // Check if current user does not have a step 'motivation' for the current job.
        $user = User::load($this->currentUser->id());
        $jobApplications = $job->field_job_applications->referencedEntities();
        foreach ($jobApplications as $application) {
          $applicationUserId = $application->user_id->getValue()[0]['target_id'];
          if ((int)$this->currentUser->id() === (int)$applicationUserId) {
            $currentApplicationId = $application->id();
            $motivation = $this->queryHelper->getStepsByIndexAndApplicationId(1, $application->id());
            if ($motivation !== FALSE && $motivation->bundle() === 'motivation_c_v') {
              $active = $application->field_app_active->value;
              $finished = $application->field_app_finished->value;
              $currentStepIndex = (int)$application->field_app_current_step_index->value + 1;
              if ($active === '1' && $finished === '0') {
                $currentStep = $this->queryHelper->getStepsByIndexAndApplicationId($currentStepIndex, $application->id());
                if ($currentStep !== 'tests') {
                  $currentStepType = $currentStep->bundle();
                } else {
                  $currentStepType = 'tests';
                }
                switch ($currentStepType) {
                  case 'motivation_c_v':
                    $form = ['#markup' => '<h3>' . $this->t_ct('We hebben uw gegevens ontvangen.') . '</h3><p>' . $this->t_ct('U wordt op de hoogte gehouden over volgende stappen per mail') . '</p>'];
                    break;
                  case 'tests':
                    if (count($this->queryHelper->getTestStepForUserAndApplication($application->id())) === 0) {
                      $form = $this->formBuilder->getForm(SjtCreateForm::class);
                    } else {
                      $form = ['#markup' => '<h3>' . $this->t_ct('We hebben uw gegevens ontvangen.') . '</h3><p>' . $this->t_ct('U wordt op de hoogte gehouden over volgende stappen per mail') . '</p>'];
                    }
                    break;
                  case 'default_step':
                    $subtype = $this->queryHelper->getCurrentStepRatingIndex($application, TRUE, FALSE);
                    if ($subtype === 'acc') {
                      $form = ['#markup' => '<h3>' . $this->t_ct('Dag ') . $user->field_emp_firstname->value . '</h3><p>' . $this->t_ct('Je wordt uitgenodigd voor een kennismakingsgesprek (bijvoorbeeld per telefoon) en wordt daarna op de hoogte gehouden van eventuele volgende stappen.') . '</p>'];
                    } else {
                      $form = ['#markup' => '<h3>' . $this->t_ct('Dag ') . $user->field_emp_firstname->value . '</h3><p>' . $this->t_ct('Je wordt uitgenodigd voor een persoonlijk gesprek. We nemen spoedig contact op om een afspraak vast te leggen.') . '</p>'];
                    }
                    break;
                }
              } else if ($active === '0' && $finished === '0') {
                  $form = ['#markup' => '<h3>' . $this->t('Helaas') . '</h3><p>' . $this->t('U bent er niet bij') . '</p>'];
              } else if ($active === '0' && $finished === '1') {
                  $form = ['#markup' => '<h3>' . $this->t('Afgerond') . '</h3><p>' . $this->t('Deze sollicitatie is afgerond.') . '</p>'];
              }
            }
          }
        }

        $returnArr = array_merge([
          '#theme' => 'application_logged_in',
          '#jobtitle' => $job->label(),
          '#jobid' => $jobId,
          '#title' => '',
          '#form' => $form,
          '#currentStepIndex' => isset($currentStepIndex) ? $currentStepIndex : NULl,
          '#tabs' => $this->queryHelper->getTabsforCurrentJobEmployee($job),
          '#applicationId' => $currentApplicationId,
          '#finished' => $finished,
          '#active' => isset($active) ? $active : NULL,
        ], $this->getSidebarForEmployee($job));
      } else {
        $form = ['#markup' => '<h3>' . $this->t('Afgerond') . '</h3><p>' . $this->t('Deze sollicitatie is afgerond.') . '</p>'];

        $returnArr = array_merge([
          '#theme' => 'application_logged_in',
          '#jobtitle' => $job->label(),
          '#jobid' => $jobId,
          '#title' => '',
          '#form' => $form,
          '#currentStepIndex' => isset($currentStepIndex) ? $currentStepIndex : NULl,
          '#tabs' => $this->queryHelper->getTabsforCurrentJobEmployee($job),
        ], $this->getSidebarForEmployee($job));
      }
      return $returnArr;
    }

    return new RedirectResponse(Url::fromRoute('<front>')->toString());
  }

  private function getSidebarForEmployee($job)
  {
    // Get the company of the job.
    $userCompany = $job->user_id->referencedEntities()[0];

    // Get the logo
    $image = NULL;
    if (isset($userCompany->get('field_company_logo')->getValue()[0])) {
      $logoFid = $userCompany->get('field_company_logo')->getValue()[0]['target_id'];
      if ($logoFid !== NULL) {
        $style = $this->entityTypeManager->getStorage('image_style')->load('logo');
        $image = $style->buildUrl(File::load($logoFid)->getFileUri());
      }
    }

    // Get the fields for the sidebar
    $fields = [
      'Gepubliceerd' => (new \DateTime('@' . $job->created->value))->format('d m Y'),
    ];
    if (isset($job->field_job_diploma->referencedEntities()[0])) {
      $fields['Gewenst diploma'] = $job->field_job_diploma->referencedEntities()[0]->label();
    }
    if (isset($job->field_job_contracttype->referencedEntities()[0])) {
      $fields['Type'] = $job->field_job_contracttype->referencedEntities()[0]->label();
    }
    if (isset($job->field_job_location[0])) {
      $fields['Locatie'] = $job->field_job_location->value;
    }

    $site = NULL;
    if (isset($userCompany->get('field_company_website')->getValue()[0]) && $userCompany->get('field_company_website')->getValue()[0]['value'] !== '') {
      $site = $userCompany->get('field_company_website')->getValue()[0]['value'];
    }
    return [
      '#logo' => $image,
      '#company' => $userCompany->field_company_company->value,
      '#jobIntro' => $job->field_job_short_desc->value,
      '#jobDesc' => $job->field_job_desc->value,
      '#fields' => $fields,
      '#companyDetails' => $this->getCompanyDetails($userCompany),
      '#companySite' => $site,
    ];
  }

  private function getCompanyDetails($userComany)
  {
    $arr = [
      'Slogan' => 'field_company_slogan',
      'Sector' => 'field_company_sector',
      'Kernactiviteit' => 'field_company_core_activity',
      'Straat + huisnummer' => 'field_company_street_number',
      'Contactpersoon' => 'field_company_contactperson',
    ];
    $fields = [];
    foreach ($arr as $key => $field) {
      if (isset($userComany->get($field)->getValue()[0]['value'])) {
        $fields[$key] = $userComany->get($field)->getValue()[0]['value'];
      }
    }
    $fields['E-mail'] = $userComany->getEmail();

    return $fields;
  }
}


