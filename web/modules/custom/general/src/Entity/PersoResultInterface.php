<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Perso result entities.
 *
 * @ingroup general
 */
interface PersoResultInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Perso result name.
   *
   * @return string
   *   Name of the Perso result.
   */
  public function getName();

  /**
   * Sets the Perso result name.
   *
   * @param string $name
   *   The Perso result name.
   *
   * @return \Drupal\general\Entity\PersoResultInterface
   *   The called Perso result entity.
   */
  public function setName($name);

  /**
   * Gets the Perso result creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Perso result.
   */
  public function getCreatedTime();

  /**
   * Sets the Perso result creation timestamp.
   *
   * @param int $timestamp
   *   The Perso result creation timestamp.
   *
   * @return \Drupal\general\Entity\PersoResultInterface
   *   The called Perso result entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Perso result published status indicator.
   *
   * Unpublished Perso result are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Perso result is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Perso result.
   *
   * @param bool $published
   *   TRUE to set this Perso result to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\general\Entity\PersoResultInterface
   *   The called Perso result entity.
   */
  public function setPublished($published);

}
