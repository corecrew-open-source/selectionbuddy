<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Motivation cv entities.
 *
 * @ingroup general
 */
class MotivationCVDeleteForm extends ContentEntityDeleteForm {


}
