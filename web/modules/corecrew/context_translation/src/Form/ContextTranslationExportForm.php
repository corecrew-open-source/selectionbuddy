<?php

namespace Drupal\context_translation\Form;

use Drupal\Component\Gettext\PoStreamWriter;
use Drupal\context_translation\ContextTranslationPoDatabaseReader;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\locale\Form\ExportForm;
use Drupal\locale\PoDatabaseReader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Form for the Gettext translation files export form.
 *
 * @internal
 */
class ContextTranslationExportForm extends ExportForm {
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // If template is required, language code is not given.
    if ($form_state->getValue('langcode') != LanguageInterface::LANGCODE_SYSTEM) {
      $language = $this->languageManager->getLanguage($form_state->getValue('langcode'));
    }
    else {
      $language = NULL;
    }
    $content_options = $form_state->getValue('content_options', []);
    $reader = new ContextTranslationPoDatabaseReader();
    $language_name = '';
    if ($language != NULL) {
      $reader->setLangcode($language->getId());
      $reader->setOptions($content_options);
      $languages = $this->languageManager->getLanguages();
      $language_name = isset($languages[$language->getId()]) ? $languages[$language->getId()]->getName() : '';
      $filename = $language->getId() . '.po';
    }
    else {
      // Template required.
      $filename = 'drupal.pot';
    }

    $item = $reader->readItem();
    if (!empty($item)) {
      $uri = $this->fileSystem->tempnam('temporary://', 'po_');
      $header = $reader->getHeader();
      $header->setProjectName($this->config('system.site')->get('name'));
      $header->setLanguageName($language_name);

      $writer = new PoStreamWriter();
      $writer->setURI($uri);
      $writer->setHeader($header);

      $writer->open();
      $writer->writeItem($item);
      $writer->writeItems($reader);
      $writer->close();

      $response = new BinaryFileResponse($uri);
      $response->setContentDisposition('attachment', $filename);
      $form_state->setResponse($response);
    }
    else {
      $this->messenger()->addStatus($this->t('Nothing to export.'));
    }
  }
}
