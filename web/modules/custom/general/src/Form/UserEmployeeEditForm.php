<?php

namespace Drupal\general\Form;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\general\Plugin\QueryHelper;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class UserEmployeeEditForm.
 */
class UserEmployeeEditForm extends FormBase {

  use ContextStringTranslationTrait;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var User
   */
  protected $currentUser;

  /**
   * Class constructor.
   * @param QueryHelper $queryHelper
   * @param User $currentUser
   */
  public function __construct(QueryHelper $queryHelper, AccountProxy $currentUser)
  {
    $this->queryHelper = $queryHelper;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('general.query_helper'),
      $container->get('current_user')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_employee_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $currentUser = $this->currentUser;
    $user = User::load($currentUser->id());
    if ($currentUser->id() === 0 || !in_array('employe', $currentUser->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    $form['forme_picture'] = [
      '#type' => 'managed_file',
      '#title' => t('Foto (optioneel)'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [2000000],
      ],
      '#theme' => 'image_widget',
      '#preview_image_style' => 'picture',
      '#upload_location' => 'public://profile-pictures',
      '#required' => FALSE,
      '#description' => '<p><strong>' . $this->t_ct('Toegelaten bestandsextensies') . ':</strong> gif, png, jpg, jpeg</br><strong>' . $this->t_ct('Toegelaten bestandsgrootte') . ':</strong> 2MB</p>',
    ];

    $form['forme_firstname'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Voornaam'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['forme_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Naam'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['e_mail'] = [
      '#type' => 'email',
      '#title' => $this->t_ct('E-mail'),
      '#weight' => '0',
      '#default_value' => $user->getEmail(),
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'password_confirm',
      '#title' => '<strong>' . $this->t_ct('Wachtwoord aanpassen') . '</strong><p>' . $this->t_ct('Geef hieronder jouw nieuw wachtwoord in.') . '</p>',
    ];
    $sexoptions = ['M', 'V', 'X'];
    $form['forme_sex'] = [
      '#type' => 'select',
      '#title' => $this->t_ct('Geslacht'),
      '#options' => array_combine($sexoptions, $sexoptions),
      '#required' => TRUE,
      '#weight' => '0',
    ];
    $form['forme_birth'] = [
      '#type' => 'date',
      '#title' => $this->t_ct('Geboortedatum'),
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['forme_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Locatie'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['forme_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Woonplaats'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['forme_linkedin'] = [
      '#type' => 'url',
      '#title' => $this->t_ct('Link naar je LinkedIn-pagina'),
      '#weight' => '0',
    ];
    $form['forme_phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Telefoonnummer'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['forme_diploma'] = [
      '#type' => 'textfield',
      '#title' => $this->t_ct('Hoogst behaalde diploma'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];

    // Set default values
    foreach ($form as $key => $value) {
      if (strpos($key, 'forme_') !== FALSE) {
        $field = $user->get(str_replace('forme_', 'field_emp_', $key))->getValue();
        if (isset($field[0])) {
          if (isset($field[0]['value'])) {
            $form[$key]['#default_value'] = $field[0]['value'];
          }
          if (isset($field[0]['target_id'])) {
            $form[$key]['#default_value'] = [$field[0]['target_id']];
          }
        }
      }
    }

    $form['delete_group'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['delete']],
    ];

    $form['delete_group']['delete_text'] = [
      '#markup' => '<strong>' . $this->t_ct('Wilt u uw profiel verwijderen?') . '</strong><p>' . $this->t_ct('Deze actie is onomkeerbaar.') . '</p>',
    ];
    $form['delete_group']['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t_ct('Verwijderen'),
      '#submit' => ['::deleteUser'],
      '#limit_validation_errors' => [],
    ];

    $form['extra'] = [
      '#markup' => '<div class="anonymous_info"><p>' . $this->t_ct('Een aantal gegevens kunnen niet bekeken worden door de werkgever bij het anonniem solliciteren.') . '<span>?</span></p>' .
        '<ul>' .
        '<li>' . $this->t_ct('Foto') . '</li>' .
        '<li>' . $this->t_ct('Voornaam') . '</li>' .
        '<li>' . $this->t_ct('Naam') . '</li>' .
        '<li>' . $this->t_ct('Geslacht') . '</li>' .
        '<li>' . $this->t_ct('Link naar je LinkedIn-pagina') . '</li>' .
        '<li>' . $this->t_ct('Leeftijd') . '</li>' .
        '<li>' . $this->t_ct('Emailadres') . '</li>' .
        '</ul></div>',
    ];

    $form['required_info'] = [
      '#markup' => '<p><span class="form-required"></span>' . $this->t_ct('Verplicht in te vullen') . '</p>',
    ];

    $form['annuleer'] = [
      '#type' => 'submit',
      '#value' => $this->t_ct('Annuleer'),
      '#submit' => ['::cancelForm'],
      '#limit_validation_errors' => [],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t_ct('Pas gegevens aan'),
      '#submit' => ['::submitForm'],
      '#attributes' => [
        'class' => [
          'button-primary'
        ]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('e_mail') !== $form['e_mail']['#default_value']) {
      $user = user_load_by_mail($form_state->getValue('e_mail'));
      if ($user !== FALSE) {
        $form_state->setError($form['e_mail'], $this->t_ct('Er bestaat al een gebruiker met dit e-mailadres'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $currentUser = \Drupal::currentUser();
    $user = User::load($currentUser->id());
    foreach ($values as $key => $value) {
      if (strpos($key, 'forme_') !== FALSE) {
        $user->set(str_replace('forme_', 'field_emp_', $key), $value);
      }
    }

    $user->set('name', $values['e_mail']);
    $user->set('mail', $values['e_mail']);
    drupal_set_message($this->t_ct('Jouw profiel is succesvol opgeslagen'));

    if ($values['password'] !== '') {
      $user->setPassword($values['password']);
    }
    $user->save();

    $form_state->setRedirect('general.user_controller_getEmployeeProfilePage');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('general.user_controller_getEmployeeProfilePage');
  }

  /**
   * {@inheritdoc}
   */
  public function deleteUser(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('general.delete_account_form');
  }

}
