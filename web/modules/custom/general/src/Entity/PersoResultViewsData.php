<?php

namespace Drupal\general\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Perso result entities.
 */
class PersoResultViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
