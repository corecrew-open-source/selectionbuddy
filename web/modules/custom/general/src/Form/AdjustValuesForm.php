<?php

namespace Drupal\general\Form;


use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\general\Entity\Value;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\general\Plugin\QueryHelper;

/**
 * Class AdjustValuesForm.
 */
class AdjustValuesForm extends FormBase
{

  use ContextStringTranslationTrait;

  /**
   * Drupal\general\Plugin\QueryHelper definition.
   *
   * @var \Drupal\general\Plugin\QueryHelper
   */
  protected $generalQueryHelper;

  /**
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var string
   */
  protected $currentLanguage;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs a new AdjustValuesForm object.
   */
  public function __construct(QueryHelper $general_query_helper, AccountProxyInterface $currentUser, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, EntityRepositoryInterface $entityRepository)
  {
    $this->generalQueryHelper = $general_query_helper;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
    $this->entityRepository = $entityRepository;
    $this->currentLanguage = $languageManager->getCurrentLanguage()->getId();
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('general.query_helper'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('entity.repository')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'adjust_values_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $textIntro = '';
    $roles = $this->currentUser->getRoles();
    $isEmployee = FALSE;
    if (in_array('company', $roles)) {
      $textIntro =  $this->t_ct('Omdat niet enkel competenties en diploma’s belangrijk zijn om na te gaan of iemand de geschikte kandidaat is voor de job, bieden we ook de mogelijkheid om na te vragen wat een kandidaat belangrijk vindt in zijn/haar job of in de werkcontext. We vragen jou als bedrijf ook om je bedrijfswaarden in te vullen. Bedrijfswaarden zijn gerelateerd aan de bedrijfscultuur en bepalen welk soort gedrag verwacht wordt van mensen en wat belangrijk is binnen jullie organisatie. Een waarde zoals “teamgericht” betekent bijvoorbeeld dat teamwork en samenwerking belangrijk zijn in jullie bedrijf bij het uitvoeren van werk en het nemen van beslissingen. Op die manier kan je nagaan of jij en je toekomstige werknemer op dezelfde golflengte zitten! Het is niet de bedoeling dat je alles als heel belangrijk scoort, denk goed na over wat het belangrijkste is bij jullie.');
    } else if (in_array('employe', $roles)) {
      $isEmployee = TRUE;
      $textIntro =  $this->t_ct('Omdat niet enkel competenties en diploma’s belangrijk zijn om na te gaan of jij de geschikte kandidaat bent voor de job, wordt hieronder gevraagd om aan te duiden hoe belangrijk onderstaande zaken zijn voor jou in je job of in de werkcontext. Op die manier streven we ernaar dat jij en je toekomstige werkgever op dezelfde golflengte zitten!');
    }

    // Add the intro.
    $form['intro'] = [
      '#markup' => '<p class="intro">' . $textIntro . '</p>',
    ];

    // Add the legend to the side of the screen.
    $legendArr = [
      1 => $this->t_ct('Niet belangrijk'),
      2 => $this->t_ct('Minder belangrijk'),
      3 => $this->t_ct('Neutraal'),
      4 => $this->t_ct('Belangrijk'),
      5 => $this->t_ct('Heel belangrijk'),
    ];
    $buildLegend = '';
    foreach ($legendArr as $key => $element) {
      $buildLegend .= '<li class="star-' . $key . '">';
      $buildLegend .= $element;
      $buildLegend .= '</li>';
    }

    $form['legend'] = [
      '#markup' => '<ul class="legend">'. $buildLegend .'</ul>',
    ];

    $form['values'] = [
      '#type' => 'container',
    ];


    // Get the values
    $valueIds = $this->generalQueryHelper->getValuesForCurrentUser();
    $values = Value::loadMultiple($valueIds);
    foreach ($values as $vid => $value) {
      if ($value->hasTranslation($this->currentLanguage)) {
        $translatedValue = $this->entityRepository->getTranslationFromContext($value, $this->currentLanguage);
        $term = $translatedValue->field_value_term->referencedEntities();
        $translatedTerm = $this->entityRepository->getTranslationFromContext($term[0], $this->currentLanguage);
        // Different description for employe then company
        $description = $translatedTerm->description->value;
        if ($isEmployee) {
          $description = $translatedTerm->field_body_employee->value;
        }
        $form['values']['value-' . $vid]['rating-' . $vid] = [
          '#type' => 'radios',
          '#title' => $translatedTerm->label(),
          '#options' => [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
          ],
          '#default_value' => $translatedValue->get('field_value_rating')->getValue()[0]['value'],
          '#description' => $description,
        ];
      }
    }

    $form['buttons'] = [
      '#type' => 'container',
    ];

    $form['buttons']['annuleer'] = [
      '#type' => 'submit',
      '#value' => $this->t('Annuleer'),
      '#submit' => ['::cancelForm'],
      '#limit_validation_errors' => [],
    ];
    $form['buttons']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Pas waarden aan'),
      '#submit' => ['::submitForm'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state)
  {
    $roles = $this->currentUser->getRoles();
    if (in_array('company', $roles)) {
      $form_state->setRedirect('general.user_controller_getCompanyProfilePage');
    } else if (in_array('employe', $roles)) {
      $form_state->setRedirect('general.user_controller_getEmployeeProfilePage');
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      if (strpos($key, 'rating-') !== FALSE) {
        $vid = str_replace('rating-', '', $key);
        $valueEntity = Value::load($vid);
        $valueEntity->set('field_value_rating', $value);
        $valueEntity->save();
      }
    }

    $roles = $this->currentUser->getRoles();
    if (in_array('company', $roles)) {
      $user = User::load($this->currentUser->id());
      $user->set('field_company_values_adjusted', '1');
      $user->save();

      $form_state->setRedirect('general.user_controller_getCompanyProfilePage');
    } else if (in_array('employe', $roles)) {
      $form_state->setRedirect('general.user_controller_getEmployeeProfilePage');
    }
  }

}
