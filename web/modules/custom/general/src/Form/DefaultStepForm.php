<?php

namespace Drupal\general\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\general\Entity\Application;
use Drupal\general\Plugin\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form controller for Default step edit forms.
 *
 * @ingroup general
 */
class DefaultStepForm extends ContentEntityForm
{

  use ContextStringTranslationTrait;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Class constructor.
   * @param EntityManagerInterface $entity_manager
   * @param EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param TimeInterface $time
   * @param QueryHelper $queryHelper
   */
  public function __construct(EntityManagerInterface $entity_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, QueryHelper $queryHelper, RequestStack $requestStack)
  {
    parent::__construct($entity_manager, $entity_type_bundle_info, $time);
    $this->queryHelper = $queryHelper;
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('entity.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('general.query_helper'),
      $container->get('request_stack')
    );
  }

  /**
   * Keep track of how many times the form
   * is placed on a page.
   *
   * @var int
   */
  protected static $instanceId;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    if (empty(self::$instanceId)) {
      self::$instanceId = 1;
    }
    else {
      self::$instanceId++;
    }

    return 'default_step_form_' . self::$instanceId;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /* @var $entity \Drupal\general\Entity\DefaultStep */
    $form = parent::buildForm($form, $form_state);

    $form['wrapper' . $this->entity->id()] = [
      '#markup' => '',
    ];

    $form['name']['widget'][0]['#title'] = t('Naam van de vacature');
    unset($form['name']['widget'][0]['#description']);
    $form['field_job_date']['widget'][0]['value']['#date_format'] = 'd/m/Y';

    $form['status']['#attributes']['class'][] = 'hidden';

    $applicationId = $this->request->get('id');
    if ($applicationId !== NULL) {
      $form['application_id'] = [
        '#type' => 'hidden',
        '#value' => $applicationId,
      ];
    }
    $form['field_step_rating']['widget'][0]['value']['#type'] = 'radios';
    $options = $form['field_step_rating']['widget'][0]['value']['#options'];
    unset($options['null']);
    unset($options[0]);
    $form['field_step_rating']['widget'][0]['value']['#options'] = $options;

    $form['index_tab'] = [
      '#type' => 'hidden',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $entity = $this->entity;
    $status = parent::save($form, $form_state);
    $application = Application::load($values['application_id']);
    $allSteps = json_decode($application->get('field_app_all_steps')->getValue()[0]['value'], TRUE);
    $indexRating = '';
    foreach ($allSteps as $index => $step) {
      if (isset($step['id']) && $step['id'] === $this->entity->id()) {
        $indexRating = $index + 1;
      }
    }

    $application->set('field_step_' . $indexRating . '_rating', $values['field_step_rating'][0]['value']);
    $application->save();

    $current_uri = $this->request->getRequestUri();
    $currentTab = $this->request->query->get('tab', FAlSE);
    if ($currentTab) {
      $current_uri = str_replace('?tab=' . $currentTab, '?tab=' . $values['index_tab'], $current_uri);
    } else {
      $current_uri .= '?tab=' . $values['index_tab'];
    }
    $form_state->setRedirectUrl(Url::fromUserInput($current_uri));
    drupal_set_message($this->t('Waarden opgeslaan'));
  }

}
