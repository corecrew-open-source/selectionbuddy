<?php

/**
 * Implements hook_theme().
 */
function general_theme()
{
  return [
    'general' => [
      'render element' => 'children',
    ],
    'company_profile_page' => [
      'variables' => [
        'company' => NULL,
        'logo' => NULL,
        'slogan' => NULL,
        'fields' => NULL,
        'values' => NULL,
      ],
    ],
    'employee_profile_page' => [
      'variables' => [
        'picture' => NULL,
        'age' => NULL,
        'gender' => NULL,
        'fields' => NULL,
        'values' => NULL,
        'name' => NULL,
      ],
    ],
    'application_anonymous' => [
      'variables' => [
        'company' => NULL,
        'jobtitle' => NULL,
        'logo' => NULL,
        'jobid' => NULL,
        'active' => NULL,
      ]
    ],
    'application_logged_in' => [
      'variables' => [
        'company' => NULL,
        'jobtitle' => NULL,
        'logo' => NULL,
        'jobid' => NULL,
        'fields' => NULL,
        'form' => NULL,
        'tabs' => NULL,
        'jobIntro' => NULL,
        'jobDesc' => NULL,
        'companyDetails' => NULL,
        'currentStepIndex' => NULL,
        'companySite' => NULL,
        'applicationId' => NULL,
        'finished' => NULL,
        'active' => NULL,
      ]
    ],
    'application_company' => [
      'variables' => [
        'profileInfo' => NULL,
        'job_id' => NULL,
        'changed' => NULL,
        'tabs' => NULL,
        'stepFields' => NULL,
        'stepExtraFields' => NULL,
        'cv' => NULL,
        'id' => NULL,
        'currentStepIndex' => NULL,
        'forms' => NULL,
        'valueCompare' => NULL,
        'lastStep' => NULL,
        'finished' => NULL,
        'employeeIsTesting' => NULL,
        'ratings' => NULL,
        'active' => NULL,
      ]
    ],
    'application_accept' => [
      'variables' => [
        'form' => NULL,
      ]
    ],
    'application_decline' => [
      'variables' => [
        'form' => NULL,
      ]
    ],
    'application_stop' => [
      'variables' => [
        'form' => NULL,
      ]
    ],
    'header_block_corecrew' => [
      'variables' => [
      ]
    ],
    'profile_block' => [
      'variables' => [
        'username' => NULL,
        'picture' => NULL,
      ]
    ],
  ];
}
