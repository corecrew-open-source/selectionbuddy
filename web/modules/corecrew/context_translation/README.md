# Context translation

A customized Drupal module to only show translations added by the CoreCrew team.
This makes it easier for the client translate things.

## Using it in Twig
Using the command in twig is pretty straightforward. 
Just use the `t_ct` twig filter instead of the `t` twig filter

Example:

```
{{ 'A string that needs translation'|t_ct }}
```

## Using it in a Controller, Form, Block or other plugin
To use the context translation it's possible to add a trait at the top of your class to be able to use the function.

For example, adding it to a controller would work like this: 

```
use ContextStringTranslationTrait;
```

```
$build = [
  '#markup' => $this->t_ct('A string that needs translation'),
];
return $build;
```

## Using it in a Hook
To use the context translation in a hook you can use the service provided by the module.

Example:

```
$contextTranslationService = \Drupal::service('context_translation.translation');
echo $contextTranslationService->t_ct("A string that needs translation");
```

