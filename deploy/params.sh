#!/bin/bash

PROJECT_NAME="hogent"
SERVER_USER="selectionbuddyeu"
SERVER_DOMAIN="ssh.selectionbuddy.eu"
SERVER_PATH="www"
IGNORE_PATH="deploy/paths.ignore"
INCLUDE_PATH="deploy/paths.include"
DRUSH_PATH='../vendor/drush/drush/drush'
CURRENT_DATETIME=`date +'%Y%m%d_%H%M%S'`
