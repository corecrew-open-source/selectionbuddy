<?php

/**
 * @file
 * Contains perso_result.page.inc.
 *
 * Page callback for Perso result entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Perso result templates.
 *
 * Default template: perso_result.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_perso_result(array &$variables) {
  // Fetch PersoResult Entity Object.
  $perso_result = $variables['elements']['#perso_result'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
