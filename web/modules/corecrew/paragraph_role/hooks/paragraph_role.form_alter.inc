<?php

/**
 * @file
 * Form alters for Default Menu Link.
 */

use \Drupal\paragraphs\Entity\ParagraphsType;
use \Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_widget_alter().
 */
function paragraph_role_field_widget_form_alter(&$element, FormStateInterface $form_state, $context)
{
  // Check if its a paragraph form.
  if (isset($element['#type']) && $element['#type'] === 'container' && isset($element['subform']['#entity_type'])) {
    if (isset($element['subform']) && $element['subform']['#entity_type']['paragraph']) {
      // Get the type en thirdpartysettings from the type.
      $type = $element['#paragraph_type'];
      $paraType = ParagraphsType::load($type);
      $roles = $paraType->getThirdPartySetting('paragraph_role', 'paragraph_roles', FALSE);

      // Filter out the options that are 0 in the setttings.
      $options = [];
      $fieldName = 'field_' . $type . '_roles';
      foreach ($roles as $role) {
        if ($role !== 0) {
          $options[$role] = ucfirst($role);
        }
      }

      // Remove the options that are not selected
      $newOptions = [];
      foreach ($options as $key => $option) {
        if (in_array($key, array_keys($element['subform'][$fieldName]['widget']['#options']))) {
          $newOptions[$key] =  $element['subform'][$fieldName]['widget']['#options'][$key];
        }
      }
      $element['subform'][$fieldName]['widget']['#options'] = $newOptions;
    }
  }
}

