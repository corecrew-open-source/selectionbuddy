<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Motivation cv entities.
 *
 * @ingroup general
 */
interface MotivationCVInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Motivation cv name.
   *
   * @return string
   *   Name of the Motivation cv.
   */
  public function getName();

  /**
   * Sets the Motivation cv name.
   *
   * @param string $name
   *   The Motivation cv name.
   *
   * @return \Drupal\general\Entity\MotivationCVInterface
   *   The called Motivation cv entity.
   */
  public function setName($name);

  /**
   * Gets the Motivation cv creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Motivation cv.
   */
  public function getCreatedTime();

  /**
   * Sets the Motivation cv creation timestamp.
   *
   * @param int $timestamp
   *   The Motivation cv creation timestamp.
   *
   * @return \Drupal\general\Entity\MotivationCVInterface
   *   The called Motivation cv entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Motivation cv published status indicator.
   *
   * Unpublished Motivation cv are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Motivation cv is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Motivation cv.
   *
   * @param bool $published
   *   TRUE to set this Motivation cv to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\general\Entity\MotivationCVInterface
   *   The called Motivation cv entity.
   */
  public function setPublished($published);

}
