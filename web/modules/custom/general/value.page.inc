<?php

/**
 * @file
 * Contains value.page.inc.
 *
 * Page callback for Value entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Value templates.
 *
 * Default template: value.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_value(array &$variables) {
  // Fetch Value Entity Object.
  $value = $variables['elements']['#value'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
