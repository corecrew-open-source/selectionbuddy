<?php

namespace Drupal\context_translation;

/**
 * Class ContextTranslationService.
 */
class ContextTranslationService {

  use ContextStringTranslationTrait {
    t_ct as t_ct_trait;
  }

  public function t_ct($string, array $args = [], array $options = []) {
    return $this->t_ct_trait($string,$args, $options);
  }
}
