<?php

namespace Drupal\general\Form;

use Drupal\context_translation\ContextStringTranslationTrait;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\general\Entity\Application;
use Drupal\general\Plugin\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RatingForm.
 */
class RatingForm extends FormBase
{

  use ContextStringTranslationTrait;

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Class constructor.
   * @param AccountInterface $account
   * @param LanguageManagerInterface $languageManager
   * @param QueryHelper $queryHelper
   * @param EntityRepositoryInterface $entityRepository
   * @param RequestStack $requestStack
   */
  public function __construct(AccountInterface $account, LanguageManagerInterface $languageManager, QueryHelper $queryHelper, EntityRepositoryInterface $entityRepository, RequestStack $requestStack)
  {
    $this->account = $account;
    $this->languageManager = $languageManager;
    $this->queryHelper = $queryHelper;
    $this->entityRepository = $entityRepository;
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('general.query_helper'),
      $container->get('entity.repository'),
      $container->get('request_stack')
    );
  }

  /**
   * Keep track of how many times the form
   * is placed on a page.
   *
   * @var int
   */
  protected static $instanceId;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    if (empty(self::$instanceId)) {
      self::$instanceId = 1;
    }
    else {
      self::$instanceId++;
    }

    return 'rating_form' . self::$instanceId;
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = '')
  {
    $form_state->disableCache();
    if ($this->account->id() === 0 || !in_array('company', $this->account->getRoles())) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    $applicationId = $this->request->get('id');
    if ($applicationId !== NULL) {
      $form['application_id'] = [
        '#type' => 'hidden',
        '#value' => $applicationId,
      ];
      $application = Application::load($applicationId);
      $value = 0;
      if (isset($application->get('field_step_' . $id . '_rating')->getValue()[0])) {
        $value = $application->get('field_step_' . $id . '_rating')->getValue()[0]['value'];
      }

      $form['index_rating'] = [
        '#type' => 'hidden',
        '#value' => $id,
      ];

      $form['index_tab'] = [
        '#type' => 'hidden',
      ];

      $form['rating'] = [
        '#type' => 'radios',
        '#title' => $this->t_ct('Beoordeling kandidaat'),
        '#options' => [
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
        ],
        '#default_value' => $value,
        '#required' => TRUE,
        '#description' => $this->t_ct('Duid aan wat je vindt van deze kandidaat door een gepast aantal sterretjes aan te duiden.'),
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t_ct('Opslaan'),
      '#submit' => ['::submitForm'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    // Get the current application by the jobId and the userid.
    $application = Application::load($values['application_id']);
    $application->set('field_step_' . $values['index_rating'] . '_rating', $values['rating']);
    $application->save();

    $current_uri = $this->request->getRequestUri();
    $currentTab = $this->request->query->get('tab', FAlSE);
    if ($currentTab) {
      $current_uri = str_replace('?tab=' . $currentTab, '?tab=' . $values['index_tab'], $current_uri);
    } else {
      $current_uri .= '?tab=' . $values['index_tab'];
    }
    $form_state->setRedirectUrl(Url::fromUserInput($current_uri));
  }
}
