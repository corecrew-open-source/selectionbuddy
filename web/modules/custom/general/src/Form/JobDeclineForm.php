<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\general\Entity\Application;
use Drupal\general\Entity\Job;
use Drupal\general\Plugin\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class JobDeclineForm.
 */
class JobDeclineForm extends FormBase
{

  /**
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var MailManager
   */
  protected $mailManager;

  /**
   * @var QueryHelper
   */
  protected $queryHelper;

  /**
   * Class constructor.
   * @param RequestStack $requestStack
   * @param MailManager $mailManager
   */
  public function __construct(RequestStack $requestStack, MailManager $mailManager, QueryHelper $queryHelper)
  {
    $this->request = $requestStack->getCurrentRequest();
    $this->mailManager = $mailManager;
    $this->queryHelper = $queryHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('plugin.manager.mail'),
      $container->get('general.query_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'job_accept_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $applicationId = $this->request->get('id');
    if ($applicationId !== NULL) {
      $form['application_id'] = [
        '#type' => 'hidden',
        '#value' => $applicationId,
      ];

      $defValue = $this->queryHelper->getDefaultValuePerstep($applicationId, 'decline');
      $form['text'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Stuur standaard mail of pas bericht aan'),
        '#default_value' => $defValue,
      ];

      $form['call'] = [
        '#type' => 'radios',
        '#default_value' => 0,
        '#options' => [
          0 => $this
            ->t('Ik stuur bovenstaande mail'),
          1 => $this
            ->t('Ik bel de sollicitant op om dit te vertellen'),
        ],
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Afwijzen'),
      '#submit' => array('::submitForm'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // Get the value from the form.
    $values = $form_state->getValues();
    $application = Application::load($values['application_id']);

    // Set the status of the application.
    $application->set('field_app_active', 0);
    $application->save();

    if ($values['call'] === '0') {
      // Get the user email.
      $to = $application->getOwner()->getEmail();

      // Message.
      $text = str_replace("\r", '', $values['text']);
      $params['message'] = $text;
      $jobId = $this->queryHelper->getJobByApplicationId($values['application_id']);
      $job = Job::load(reset($jobId));
      $params['reply-to'] = $job->getOwner()->getEmail();
      $params['subject'] = t('@company - @job : Helaas, je mag niet door naar de volgende ronde.', ['@company' => $job->getOwner()->get('field_company_company')->getValue()[0]['value'], '@job' => $job->getName()]);

      // Send mail to employee.
      $result = $this->mailManager->mail('general', 'decline_employee_alert', $to, 'nl', $params, NULL, TRUE);
      if ($result['result'] === TRUE) {
        $message = t('A message has been send to @email', ['@email' => $to]);
        // Drupal dblog.
        \Drupal::logger('decline_employee_alert')->error($message);
        drupal_set_message($message);

        // redirect back to the detail page of the application.
      } else {
        $current_uri = $this->request->getRequestUri();
        $form_state->setRedirectUrl(Url::fromUserInput($current_uri));
      }
    } else {
      drupal_set_message($this->t('Kandidaat is afgewezen, vergeet deze niet op te bellen.'));
    }

    $form_state->setRedirect('general.application_company_controller_getApplicationDetail', ['id' => $values['application_id']]);

    return;
  }
}
